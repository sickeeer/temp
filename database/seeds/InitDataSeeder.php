<?php

use Illuminate\Database\Seeder;

class InitDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        \DB::table('sku')->insert([
            [
                'pack_num' => '1',
                'id'=> '100001',
                'name' => ' 二层直涂皮胚',
                'category_id' => 1,
                'unit' => '英尺',
            ],
        ]);

        \DB::table('category')->insert([
            [
                'title' => '二层皮胚',
                'batchattribute' => '["origin","avg_size","min_size"]'
            ],
        ]);



         \DB::table('batchattribute')->insert([
            [
                'parent_id' => 'grade',
                'name' => 'TR1',
                'value' => 'TR1',
            ],

            [
                'parent_id' => 'thick',
                'name' => '0.9-1.1',
                'value' => '0.9-1.1',
            ],

            [
                'parent_id' => 'origin',
                'name' => '河南',
                'value' => '河南',
            ],

            [
                'parent_id' => 'colour',
                'name' => '棕色',
                'value' => '棕色',
            ],

            [
                'parent_id' => 'avg_size',
                'name' => '60',
                'value' => '60',
            ],

            [
                'parent_id' => 'min_size',
                'name' => '30',
                'value' => '30',
            ],
        ]);

         \DB::table('company')->insert([
            [
                'name' => '江苏思凯林国际贸易',
            //     'country' => null,
            //     'province' => null,
                'address' => '江苏',
            //     'postcode' => null,
                'status' => '0',
            //     'tel' => null,
            //     'linkman' => null,
            //     'bankinfo' => null,
            //     'created_at' => null,
            //     'updated_at' => null,

            ],
        ]);

        \DB::table('agent')->insert([
            [
                'name' => '思凯林商丘皮厂',
                // 'country'=> unll,
                // 'province'=> null,
                'address' => '河南省商丘市',
                // 'contacts_person' => null,
                // 'phone' => null,
                // 'email' => null,
                // 'faxes' => null,
                'out_tax' => '1.0000',
                // 'taxpayer_id' => null,
                // 'fpArea' => null,
                // 'fpPhone' =>null,
                'customer_source' => '0',
                'salesman' => 'xsadmin',
                'merchandiser' => 'gsadmin',
                //'remark' => null,
                'status' => 0,
                'created_at' => '2018-05-02 05:51:59',
                'updated_at' => '2018-05-02 05:51:59',

            ],
            [
                'name' => '艾贝思凯',
                // 'country'=> unll,
                // 'province'=> null,
                'address' => '江苏省昆山市',
                // 'contacts_person' => null,
                // 'phone' => null,
                // 'email' => null,
                // 'faxes' => null,
                'out_tax' => '1.0000',
                // 'taxpayer_id' => null,
                // 'fpArea' => null,
                // 'fpPhone' =>null,
                'customer_source' => '0',
                'salesman' => 'xsadmin',
                'merchandiser' => 'gsadmin',
                //'remark' => null,
                'status' => 0,
                'created_at' => '2018-05-02 05:52:55',
                'updated_at' => '2018-05-02 05:51:59',
            ],
        ]);

        \DB::table('agent_sell_price_bill')->insert([
            [
            'id' => 'C201805020001',
            // 'start_time' => null,
            // 'end_time' => null,
            'operater_id' => 'sickeeer',
            'created_at' => '2018-05-02 05:56:34',
            //'updated_at' => null,
            ],

            [
            'id' => 'C201805020002',
            // 'start_time' => null,
            // 'end_time' => null,
            'operater_id' => 'sickeeer',
            'created_at' => '2018-05-02 05:57:34',
            //'updated_at' => null,
            ],

            [
            'id' => 'C201805020003',
            // 'start_time' => null,
            // 'end_time' => null,
            'operater_id' => 'sickeeer',
            'created_at' => '2018-05-02 07:00:32',
            //'updated_at' => null,
            ],
        ]);

        \DB::table('agent_sell_price_bill_detail')->insert([
            [
                'sku_id' => '100001',
                'agent_sell_price_bill_id' => 'C201805020001',
                'datam_id' => '0',
                'datam' => '默认',
                'datam_type' => '0',
                'price' => '3.9000',
                'enable' => '0',
                //'remark' => null,
                'created_at' => '2018-05-02 05:56:34',
                //'updated_at' => null,
            ],

            [
                'sku_id' => '100001',
                'agent_sell_price_bill_id' => 'C201805020002',
                'datam_id' => '7',
                'datam' => '艾贝思凯',
                'datam_type' => '1',
                'price' => '8.0000',
                'enable' => '0',
                //'remark' => null,
                'created_at' => '2018-05-02 05:57:34',
                //'updated_at' => null,
            ],

            [
                'sku_id' => '100001',
                'agent_sell_price_bill_id' => 'C201805020003',
                'datam_id' => '7',
                'datam' => '艾贝思凯',
                'datam_type' => '1',
                'price' => '15.0000',
                'enable' => '0',
                //'remark' => null,
                'created_at' => '2018-05-02 07:00:32',
                //'updated_at' => null,
            ],
        ]);

        \DB::table('supplier')->insert([
            [
                'name' => '河北开阳皮革有限公司',
                'address' => '河北省',
                'type' => 0,
                'rate' => 1,
                'currency' => '0'
            ],
            [
                'name' => '河北威盛皮革有限公司',
                'address'=> '河北省',
                'type' => 0,
                'rate' => 1,
                'currency' => '0'
            ],
            [
                'name' => '思凯林商丘皮厂',
                'address'=> '河南省商丘市',
                'type' => 0,
                'rate' => 1,
                'currency' => '0'
            ]
        ]);

        \DB::table('supplier_goods_price_bill')->insert([
            [
                'id' => 'S'.date('Ymd')."0001",
                'sku_id' => '100001',
                'supplier_id' => '1',
                'cycle' => 15,
                'price' => 100,
            ],
        ]);
/*
        $oid = 'O201805060001';
        \DB::table('order')->insert([
            [
                'id'=> $oid,
                'type' => '0',
                'agent_id' => '1',
                'send_time' =>date('Y-m-d H:i:s'),
                'logistics' => '0',
                'pay_type' => '0',
                'is_send' => '0',
                'remark' => ' ',
                'total_price'=>'700',
                'operater_id' => 'sickeeer',
                'company_id'=>1,
                'actual_money'=>'700',
                'rate' => 1,
                'tax' => 7
            ],
        ]);

        \DB::table('order_detail')->insert([

            [
                'sku_id' => '100001',
                'num' => '3.0000',
                'size' => '3.5',
                'price' => '200',
                'grade' => 1,
                'thick' => 4,
                'colour' => 2,
                'min_size' => 3,
                'avg_size' => 7,
                'order_id' => $oid,

            ],
        ]);

        $pid = 'P201805060001';
        \DB::table('purchase')->insert([
            [
                'id'=> $pid,
                'supplier_id' => "2",
                'company_id' => 1,
                'operater_id' => 'sickeeer',
                'total_amount' => '1000',
                'rate' => '5',
                'tax' => '50',
                'exchange_rate' => '1',
                'exchange_amount' => '1000',
                'warehouse_id' => 'SH0001',
                'type' => '1',
                'created_at'=>date('Y-m-d H:i:s')
            ],
        ]);

        \DB::table('purchase_detail')->insert([
            [
                'purchase_id' => $pid,
                'sku_id' => '100001',
                'price' => '100',
                'num' => '4',
                'size' => '4',
                'grade'=> '1',
                'colour'=> 4,
                'supplier_product_id'=>'S201805060001',
                'arrive_time' => date('Y-m-d H:i:s'),
                // 'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
*/
        \DB::table('warehouse')->insert([
            [
                'id' => 'KS0001',
                'name' => '昆山一仓',
                'company_id' => 1,
                'address' => '昆山某地',
            ],
        ]);

        \DB::table('warehouse_zone')->insert([
            'id' => 'KS0001-001',
            'name' => '昆山仓一库区',
            'warehouse_id'=> 'KS0001',
            'created_at' =>date('Y-m-d H:i:s'),
        ]);

        \DB::table('warehouse_location')->insert([
            'id' => '001',
            'name' => '昆山仓一库区一库位',
            'warehouse_zone_id' => 'KS0001-001',
            'warehouse_location_type' => 1,
            'created_at' =>date('Y-m-d H:i:s'),
        ]);

    }
    public function down()
    {


        \DB::table('agent')->truncate();
        \DB::table('agent_sell_price_bill')->truncate();
        \DB::table('agent_sell_price_bill_detail')->truncate();

        \DB::table('order')->truncate();
        \DB::table('order_detail')->truncate();

        \DB::table('sku')->truncate();
        \DB::table('company')->truncate();
        \DB::table('batchattribute')->truncate();
        \DB::table('category')->truncate();
        \DB::table('warehouse')->truncate();
        \DB::table('warehouse_zone')->truncate();
        \DB::table('warehouse_location')->truncate();
        \DB::table('supplier')->truncate();
        \DB::table('supplier_goods_price_bill')->truncate();

        \DB::table('purchase')->truncate();
        \DB::table('purchase_detail')->truncate();
    }
}
