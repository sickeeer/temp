<?php

use Illuminate\Database\Seeder;

class CommonModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('common_model')->insert([
        //     [
        //         'common_key'=>'sku_batch_attribute',
        //         'common_des'=>'批次属性',
        //         'common_value'=>serialize([
        //             'colour'=>'颜色',
        //             'grade'=>'等级',
        //             'origin'=>'产地',
        //             'thick'=>'厚度',
        //             'avg_size'=>'平均尺寸',
        //             'min_size'=>'最小尺寸'
        //         ]),
        //     ],
        //     /**
        //     [
        //         'common_key'=>'company_status',
        //         'common_des'=>'公司状态',
        //         'common_value'=>serialize([

        //         ]),
        //     ],
        //     **/
        // ]);
        \DB::table('id_model')->insert([
            [
                'common_key' => "agent_sell_price_bill_id",
                'common_des' => "客户调价单据自增ID",
                'common_value' => 1,
                'frequency' => 'daily'
            ],
            [
                'common_key' => 'supplier_goods_price_bill_id',
                'common_des' => "供应商调价单据自增ID",
                'common_value' => 6,
                'frequency' => 'daily'
            ],
            [
                'common_key' => 'purchase_id',
                'common_des' => "采购单据自增ID",
                'common_value' => 3,
                'frequency' => 'daily'
            ],
            [
                'common_key' => 'order_id',
                'common_des' => "销售单据自增ID",
                'common_value' => 3,
                'frequency' => 'daily'
            ],
            [
                'common_key' => 'batch_id',
                'common_des' => "批次自增ID",
                'common_value' => 3,
                'frequency' => 'daily'
            ],
            [
                'common_key' => 'tansfer_bill_id',
                'common_des' => "调度单据自增ID",
                'common_value' => 3,
                'frequency' => 'daily'
            ],
        ]);
    }

    public function down()
    {
        // DB::table('common_model')->truncate();
        DB::table('id_model')->truncate();
    }
}
