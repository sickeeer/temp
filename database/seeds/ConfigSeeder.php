<?php

use Illuminate\Database\Seeder;
class ConfigSeeder  extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_config')->truncate();
        \DB::table('admin_config')->insert([
            [
                'name' => 'state',
                'value' => json_encode([
                    '0' => ['class'=>"label label-success",'value'=>'正常'],
                    '1' => ['class'=>"label label-warning",'value'=>'冻结'],
                    '2' => ['class'=>"label label-danger",'value'=>'删除'],
                ]),
                'description' => '状态'
            ],

            [
                'name' => 'batchAttribute',
                'value' => json_encode([
                    'grade' => '等级',
                    'thick' => '厚度',
                    'colour' => '颜色',
                    'origin' => '产地',
                    'avg_size' => '平均尺寸',
                    'min_size' => '最小尺寸',
                ]),
                'description' => '批次属性'
            ],

            [
                'name' => 'batchAttributeState',
                'value' => json_encode([
                        'on'  => ['value' => 0, 'text' => '正常', 'color' => 'success'],
                        'off' => ['value' => 1, 'text' => '冻结', 'color' => 'danger'],
                      ]),
                'description' => '批次属性维护'
            ],

            [
                'name' => 'membership',
                'value' => json_encode([
                    '0' => ['class'=>"label label-success",'value'=>'默认'],
                    // '1' => ['class'=>"label label-danger",'value'=>'会员'],
                    // '2' => ['class'=>"label label-info",'value'=>'个人']
                ]),
                'description' => '调价单位'
            ],

            [
                'name' => 'agentType',
                'value' => json_encode([
                    '0' => '外部客户',
                    '1' => '内部客户',
                ]),
                'description' => '客户类型'
            ],

            [
                'name' => 'supplierType',
                'value' => json_encode([
                    '0' => '外部供应商',
                    '1' => '内部供应商',
                ]),
                'description' => '供应商类型'
            ],

            [
                'name' => 'purchaseType',
                'value' => json_encode([
                    '0' => '公司采购',
                    '1' => '外协采购',
                ]),
                'description' => '采购类型'
            ],

            [
                'name' => 'orderType',
                'value' => json_encode([
                    '0' => '销售单',
                    '1' => '外协出库单',
                ]),
                'description' => '订单类型'
            ],

            [
                'name' => 'warehouseLocationType',
                'value' => json_encode([
                    '0' => ['class'=>"label label-success",'value'=>'正常'],
                    '1' => ['class'=>"label label-danger",'value'=>'退货'],
                    '2' => ['class'=>"label label-warning",'value'=>'残次']
                ]),
                'description' => '库位类型'
            ],

            [
                'name' => 'datamType',
                'value' => json_encode([
                    '0' => '会员调价',
                    '1' => '特定客户调价',
                ]),
                'description' => '调价类型'
            ],

            [
                'name' => 'type',
                'value' => json_encode([
                    '0' => '客户等级',
                    '1' => '特定客户',
                ]),
                'description' => '调价方式'
            ],

            [
                'name' => 'inType',
                'value' => json_encode([
                    '0' => '采购单',
                    '1' => '外协入库单',
                ]),
                'description' => '订单类型'
            ],

            [
                'name' => 'inState',
                'value' => json_encode([
                    'CREATED' => ['class'=>"label label-success",'value'=>'待处理'],
                    'PART_IN' => ['class'=>"label label-warning",'value'=>'部分入库'],
                    'ALL_IN' => ['class'=>"label label-primary",'value'=>'全部入库'],
                ]),
                'description' => '入库状态'
            ],

            [
                'name' => 'customerSource',
                'value' => json_encode([
                    '0' => '默认',
                    //'1' => '自提',
                ]),
                'description' => '客户来源'
            ],

            [
                'name' => 'currency',
                'value' => json_encode([
                    'CNY' => [
                        'exchange'=>'1',
                        'value'=>'人民币'
                    ],
                    'USD' => [
                        'exchange'=>'1',
                        'value'=>'美元'
                    ],
                ]),
                'description' => '币种'
            ],

            [
                'name' => 'Logistics',
                'value' => json_encode([
                    '0' => '物流',
                    '1' => '自提',
                    '2' => '其他'
                ]),
                'description' => '物流方式'
            ],

            [
                'name' => 'payType',
                'value' => json_encode([
                    '0' => ['class'=>"label label-success",'value'=>'待维护'],
                    '1' => ['class'=>"label label-success",'value'=>'现金'],
                    '2' => ['class'=>"label label-success",'value'=>'电汇'],
                    '3' => ['class'=>"label label-success",'value'=>'支票'],
                    '4' => ['class'=>"label label-success",'value'=>'其他']
                ]),
                'description' => '支付方式'
            ],
            [
                'name' => 'paymentState',
                'value' => json_encode([
                    '0' => ['class'=>"label label-danger",'value'=>'未付款'],
                    '1' => ['class'=>"label label-warning",'value'=>'部分付款'],
                    '2' => ['class'=>"label label-success",'value'=>'付全款'],
                    '3' => ['class'=>"label label-default",'value'=>'未知']
                ]),
                'description' => '支付状态'
            ],

            [
                'name'=>'purchaseState',
                'value'=>json_encode([
                    'CREATED' => ['class'=>"label label-success",'value'=>'已生成'],
                    'CHARGE_CHECK' => ['class'=>"label label-info",'value'=>'已提交'],
                    'PART_IN' => ['class'=>"label label-warning",'value'=>'部分入库'],
                    'ALL_IN' => ['class'=>"label label-primary",'value'=>'全部入库'],
                    'INACTIVED' => ['class'=>"label label-danger",'value'=>'已取消'],
                    'FINISHED' => ['class'=>"label label-success",'value'=>'完成']
                ]),
                'description' => '采购单审核状态'
            ],

            [
                'name'=>'purchaseReviewState',
                'value'=>json_encode([
                    'CREATED' => ['class'=>"label label-warning",'value'=>'未审核'],
                    'CHARGE_CHECK' => ['class'=>"label label-warning",'value'=>'主管审核'],
                    'FINANCE_1ST' => ['class'=>"label label-primary",'value'=>'财务审核'],
                    'IN_CHECK' => ['class'=>"label label-primary",'value'=>'入库审核'],
                    'FINANCE_LAST' => ['class'=>"label label-danger",'value'=>'财务确认'],
                    'INACTIVED' => ['class'=>"label label-danger",'value'=>'取消'],
                    'FINISHED' => ['class'=>"label label-success",'value'=>'完成']
                ]),
                'description' => '入库审核状态'
            ],

            [
                'name' => 'orderState',
                'value' => json_encode([
                    'CREATED' => ['class'=>"label label-primary",'value'=>'已生成'],
                    'CHARGE_CHECK' => ['class'=>"label label-success",'value'=>'已提交'],
                    'PART_OUT' => ['class'=>"label label-warning",'value'=>'部分出库'],
                    'ALL_OUT' => ['class'=>"label label-primary",'value'=>'全部出库'],
                    'INACTIVED' => ['class'=>"label label-danger",'value'=>'已取消'],
                    'FINISHED' => ['class'=>"label label-success",'value'=>'完成']
                ]),
                'description'=>'销售单状态'
            ],

            [
                'name'=>'orderReviewState',
                'value'=>json_encode([
                    'CREATED' => ['class'=>"label label-danger",'value'=>'未审核'],
                    'CHARGE_CHECK' => ['class'=>"label label-warning",'value'=>'主管审核'],
                    'FINANCE_1ST' => ['class'=>"label label-primary",'value'=>'财务审核'],
                    'OUT_CHECK' => ['class'=>"label label-primary",'value'=>'出库审核'],
                    'FINANCE_LAST' => ['class'=>"label label-danger",'value'=>'财务确认'],
                    'INACTIVED' => ['class'=>"label label-danger",'value'=>'取消'],
                    'FINISHED' => ['class'=>"label label-success",'value'=>'完成']
                ]),
                'description' => '出库审核状态'
            ],

            [
                'name' => 'inventoryCheckingDetail',
                'value' => json_encode([
                    '1' => ['class'=>"label label-primary",'value'=>'出库'],
                    '2' => ['class'=>"label label-info",'value'=>'入库'],
                    '3' => ['class'=>"label label-danger",'value'=>'出库锁定'],
                    '4' => ['class'=>"label label-warning",'value'=>'入库在途'],
                    '5' => ['class'=>"label label-danger",'value'=>'调拨锁定'],
                    '6' => ['class'=>"label label-warning",'value'=>'调拨在途'],
                    '103' => ['class'=>"label label-danger",'value'=>'出库锁定释放'],
                    '104' => ['class'=>"label label-warning",'value'=>'入库在途释放'],
                    '105' => ['class'=>"label label-danger",'value'=>'调拨锁定释放'],
                    '106' => ['class'=>"label label-warning",'value'=>'调拨在途释放'],
                ]),
                'description' => '库位类型'
            ],

            [
                'name' => 'outState',
                'value' => json_encode([
                    'CREATED' => ['class'=>"label label-success",'value'=>'待处理'],
                    'PART_OUT' => ['class'=>"label label-warning",'value'=>'部分出库'],
                    'ALL_OUT' => ['class'=>"label label-primary",'value'=>'全部出库'],
                ]),
                'description'=>'出库状态'
            ],

            [
                'name' => 'transferBillState',
                'value' => json_encode([
                    'CREATED' => ['class'=>"label label-success",'value'=>'已生成'],
                    // 'CHARGE_OUT_CHECK' => ['class'=>"label label-warning",'value'=>'等待出库'],
                    'PART_OUT' => ['class'=>"label label-info",'value'=>'部分出库'],
                    'ALL_OUT' => ['class'=>"label label-primary",'value'=>'全部出库'],
                    // 'CHARGE_IN_CHECK' => ['class'=>"label label-warning",'value'=>'等待入库'],
                    'PART_IN' => ['class'=>"label label-info",'value'=>'部分入库'],
                    'ALL_IN' => ['class'=>"label label-primary",'value'=>'全部入库'],
                    'INACTIVED' => ['class'=>"label label-danger",'value'=>'已取消'],
                    'FINISHED' => ['class'=>"label label-success",'value'=>'完成']
                ]),
                'description' => '调拨单状态'
            ],
            [
                'name' => 'transferBillReviewState',
                'value' => json_encode([
                    'CREATED' => ['class'=>"label label-success",'value'=>'未审核'],
                    'CHARGE_OUT_CHECK' => ['class'=>"label label-warning",'value'=>'主管出库审核'],
                    'OUT_CHECK' => ['class'=>"label label-primary",'value'=>'出库审核'],
                    'CHARGE_IN_CHECK' => ['class'=>"label label-success",'value'=>'主管入库审核'],
                    'IN_CHECK' => ['class'=>"label label-success",'value'=>'入库审核'],
                    'INACTIVED' => ['class'=>"label label-danger",'value'=>'取消']
                ]),
                'description' => '调拨单审核状态'
            ],
        ]);
    }
    public function down()
    {
        DB::table('admin_config')->truncate();
    }
}
