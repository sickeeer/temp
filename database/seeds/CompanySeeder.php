<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('company')->insert(
            [
                'country'=>'中国',
                'province'=>'江苏',
                'address'=>'思凯林国际贸易',
                'name'=>'江苏思凯林国际贸易'
            ]
        );
    }
    public function down()
    {
        DB::table('company')->truncate();
    }
}
