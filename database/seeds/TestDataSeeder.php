<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oid = 'O'.date('Ymd')."0001";
        \DB::table('order')->insert([
            [
                'id'=> $oid,
                'type' => '0',
                'agent_id' => '1',
                'send_time' =>date('Y-m-d H:i:s'),
                'logistics' => '0',
                'pay_type' => '0',
                'is_send' => '0',
                'remark' => ' ',
                'total_price'=>'700',
                'operater_id' => 'sickeeer',
                'company_id'=>1,
                'actual_money'=>'700'
            ],
        ]);

         \DB::table('sku')->insert([
            [
                'pack_num' => '1',
                'id'=> 'LP',
                'name' => '蓝皮',
                'category_id' => 1,
                'unit' => '英尺',
            ],
            [
                'pack_num' => '1',
                'id'=> 'PP',
                'name' => '皮胚',
                'category_id' => 2,
                'unit' => '英尺',
            ],
            [
                'pack_num' => '1',
                'id'=> 'FT',
                'name' => '缝套',
                'category_id' => 3,
                'unit' => '英尺',
            ]
        ]);

         \DB::table('category')->insert([
            [
                'title' => '蓝皮',
                'batchattribute' => '["grade","avg_size"]'
            ],
            [
                'title' => '皮胚',
                'batchattribute' => '["grade","min_size"]'
            ],
            [
                'title' => '缝套',
                'batchattribute' => '["colour","avg_size"]'
            ]
        ]);

         \DB::table('batchattribute')->insert([
            [
                'code' => 'grade',
                'name' => 'E级',
                'value' => 'E',
            ],

            [
                'code' => 'grade',
                'name' => 'D1级',
                'value' => 'D1',
            ],

            [
                'code' => 'grade',
                'name' => '5级',
                'value' => '5',
            ],

            [
                'code' => 'thick',
                'name' => '0.9-1.1',
                'value' => '0.9-1.1',
            ],

            [
                'code' => 'thick',
                'name' => '1.0-1.2',
                'value' => '1.0-1.2',
            ],

            [
                'code' => 'thick',
                'name' => '1.6-1.8',
                'value' => '1.6-1.8',
            ],

            [
                'code' => 'origin',
                'name' => '河南',
                'value' => '河南',
            ],

            [
                'code' => 'origin',
                'name' => '昆山',
                'value' => '昆山',
            ],

            [
                'code' => 'origin',
                'name' => '烟台',
                'value' => '烟台',
            ],

            [
                'code' => 'colour',
                'name' => '棕色',
                'value' => '棕色',
            ],

            [
                'code' => 'colour',
                'name' => '米黄色',
                'value' => '米黄色',
            ],

            [
                'code' => 'colour',
                'name' => '咖啡色',
                'value' => '咖啡色',
            ],

            [
                'code' => 'avg_size',
                'name' => '100',
                'value' => '100',
            ],

            [
                'code' => 'avg_size',
                'name' => '50',
                'value' => '50',
            ],

            [
                'code' => 'avg_size',
                'name' => '25',
                'value' => '25',
            ],

            [
                'code' => 'min_size',
                'name' => '80',
                'value' => '80',
            ],

            [
                'code' => 'min_size',
                'name' => '40',
                'value' => '40',
            ],

            [
                'code' => 'min_size',
                'name' => '20',
                'value' => '20',
            ],
        ]);

        \DB::table('agent')->insert([
            [
                'name' => '上海某牛皮贸易公司',
                'address'=> '上海',
                'out_tax'=> 10
            ],
            [
                'name' => '广州某牛皮贸易公司',
                'address'=> '广州',
                'out_tax'=> 20
            ],
            [
                'name' => '国外某牛皮贸易公司',
                'address'=> '国外',
                'out_tax'=> 8
            ]
        ]);

        \DB::table('supplier')->insert([
            [
                'name' => '上海某牛皮供应商',
                'type'=> 0,
                'rate'=> 12,
                'address' => '上海',
                'currency' => '0'
            ],
            [
                'name' => '广州某牛皮供应商',
                'type' => 0,
                'address'=> '广州',
                'rate'=> 20,
                'currency' => '0'
            ],
            [
                'name' => '国外某牛皮供应商',
                'type' => 0,
                'address'=> '国外',
                'rate'=> 8,
                'currency' => '1'
            ]
        ]);

        \DB::table('purchase_detail')->insert([
            [
                'purchase_id' => $pid,
                'sku_id' => 'LP',
                'price' => '100',
                'num' => '4',
                'grade'=> '1',
                'colour'=> 6,
                'supplier_product_id'=>'S'.date('Ymd')."0001",
                'arrive_time' => date('Y-m-d H:i:s'),
                // 'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'purchase_id' => $pid,
                'sku_id' => 'PP',
                'price' => '300',
                'num' => '2',
                'avg_size'=> '1',
                'colour'=> 4,
                'supplier_product_id'=>'S'.date('Ymd')."0003",
                'arrive_time' => date('Y-m-d H:i:s'),
                // 'created_at' => date('Y-m-d H:i:s')
            ],

        ]);

        \DB::table('order_detail')->insert([
            [
                "sku_id" => 'PP',
                'num' => '1.0000',
                'price' => '100',
                'grade' => 1,
                'thick' => 4,
                'colour' => 2,
                'min_size' => 3,
                'avg_size' => 7,
                'order_id' => $oid
            ],
            [
                'sku_id' => 'LP',
                'num' => '3.0000',
                'price' => '200',
                'grade' => 1,
                'thick' => 4,
                'colour' => 2,
                'min_size' => 3,
                'avg_size' => 7,
                'order_id' => $oid
            ],
        ]);

        \DB::table('warehouse')->insert([
            [
                'warehouse_id' => 'SH0001',
                'name' => '上海一仓',
                'company_id' => 1,
                'address' => '上海某地'
            ],
        ]);

        \DB::table('warehouse_zone')->insert([
            'warehouse_zone_id' => 'SH0001-001',
            'name' => '上海某牛皮供应商',
            'warehouse_id'=> 'SH0001',
            'created_at' =>date('Y-m-d H:i:s')
        ]);

        \DB::table('warehouse_location')->insert([
            'warehouse_location_id' => '001',
            'name' => '上海某牛皮供应商',
            'warehouse_zone_id' => 'SH0001-001',
            'warehouse_location_type' => 1,
            'created_at' =>date('Y-m-d H:i:s')
        ]);

        \DB::table('supplier_goods_price_bill')->insert([
            [
                'id' => 'S'.date('Ymd')."0001",
                'sku_id' => 'LP',
                'supplier_id' => '1',
                'cycle' => 15,
                'price' => 100,
            ],
            [
                'id' => 'S'.date('Ymd')."0002",
                'sku_id' => 'LP',
                'supplier_id' => '2',
                'cycle' => 15,
                'price' => 200,
            ],
            [
                'id' => 'S'.date('Ymd')."0003",
                'sku_id' => 'PP',
                'supplier_id' => '2',
                'cycle' => 15,
                'price' => 250,
            ],
            [
                'id' => 'S'.date('Ymd')."0004",
                'sku_id' => 'FT',
                'supplier_id' => '3',
                'cycle' => 15,
                'price' => 400,
            ],
            [
                'id' => 'S'.date('Ymd')."0005",
                'sku_id' => 'FT',
                'supplier_id' => '1',
                'cycle' => 15,
                'price' => 800,
            ],
        ]);
        $cid = 'C'.date('Ymd').str_pad(1,4,"0",STR_PAD_LEFT);
        \DB::table('agent_sell_price_bill')->insert([
            [
                'id' => $cid,
                // 'start_time' => date('Y-m-d H:i:s'),
                // 'end_time' => "9999-12-31 00:00:00",
                'created_at' => date('Y-m-d H:i:s'),
                'operater_id' => 'sickeeer'
            ],
        ]);
        \DB::table('agent_sell_price_bill_detail')->insert([
            'agent_sell_price_bill_id' => $cid,
            'sku_id' => "LP",
            'datam_type' => 0,
            'datam_id' => 0,
            'datam' => "默认",
            'price' => 400,
            'enable' => 0,
            'created_at' => date('Y-m-d H:i:s')
        ]);

    }
    public function down()
    {
        DB::table('agent')->truncate();
        DB::table('sku')->truncate();
        DB::table('supplier')->truncate();
        DB::table('category')->truncate();
        DB::table('warehouse')->truncate();
        DB::table('supplier_goods_price_bill')->truncate();

    }
}
