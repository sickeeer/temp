<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminTablesSeeder::class);
        $this->call(CommonModelSeeder::class);
        $this->call(CompanySeeder::class);
        // $this->call(TestDataSeeder::class);
        // $this->call(InitDataSeeder::class);
        $this->call(ConfigSeeder::class);

    }
}
