<?php

use Illuminate\Database\Seeder;

class AdminPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admin_permissions')->truncate();

        $permissions= [
            [ '超级权限', '*', '', '*', null, null],
            [ '仪表盘', 'dashboard', 'GET', '/', null, null],
            [ '登录', 'auth.login', '', '/auth/login', null, null],
            [ '登出', 'auth.logout', '', '/auth/logout', null, null],
            [ '用户资料修改', 'auth.setting', 'GET,PUT', '/auth/setting', null, null],
            [ '用户角色', 'auth.roles', 'GET,PUT', '/auth/roles',null,null],
            [ '权限目录','auth.permissions','GET,PUT','/auth/permissions',null,null],
            [ '目录管理','auth.menu', 'GET,PUT','/auth/menu',null,null],

            [ '商品列表','sku.list','GET','/skus',null,null],
            [ '商品列表(编辑页面)','sku.edit.html','GET','/skus/*/edit',null,null],
            [ '商品列表(新增页面)','sku.create.html','GET','/skus/create',null,null],
            [ '商品列表(编辑)','sku.edit','PUT','/skus/*',null,null],
            [ '商品列表(删除)','sku.delete','DELETE','/skus/*',null,null],
            [ '商品列表(新增)','sku.create','POST','/skus',null,null],

            [ '商品分类','category.list','GET','/categories',null,null],
            [ '商品分类(编辑页面)','category.edit.html','GET','/categories/*/edit',null,null],
            [ '商品分类(新增页面)','category.create.html','GET','/categories/create',null,null],
            [ '商品分类(编辑)','category.edit','PUT','/categories/*',null,null],
            [ '商品分类(删除)','category.delete','DELETE','/categories/*',null,null],
            [ '商品分类(新增)','category.create','POST','/categories',null,null],

            [ '批次属性维护','batchattribute.list','GET','/batchattributes',null,null],
            [ '批次属性维护(编辑页面)','batchattribute.edit.html','GET','/batchattributes/*/edit',null,null],
            [ '批次属性维护(新增页面)','batchattribute.create.html','GET','/batchattributes/create',null,null],
            [ '批次属性维护(编辑)','batchattribute.edit','PUT','/batchattributes/*',null,null],
            [ '批次属性维护(删除)','batchattribute.delete','DELETE','/batchattributes/*',null,null],
            [ '批次属性维护(新增)','batchattribute.create','POST','/batchattributes',null,null],

            [ '客户管理','agent.list','GET','/agents',null,null],
            [ '客户管理(编辑页面)','agent.edit.html','GET','/agents/*/edit',null,null],
            [ '客户管理(新增页面)','agent.create.html','GET','/agents/create',null,null],
            [ '客户管理(编辑)','agent.edit','PUT','/agents/*',null,null],
            [ '客户管理(删除)','agent.delete','DELETE','/agents/*',null,null],
            [ '客户管理(新增)','agent.create','POST','/agents',null,null],


            [ '销售价维护','skuPrice.list','GET','/skuPrices',null,null],
            [ '销售价维护(编辑页面)','skuPrice.edit.html','GET','/skuPrices/*/edit',null,null],
            [ '销售价维护(新增页面)','skuPrice.create.html','GET','/skuPrices/create',null,null],
            [ '销售价维护(编辑)','skuPrice.edit','PUT','/skuPrices/*',null,null],
            [ '销售价维护(删除)','skuPrice.delete','DELETE','/skuPrices/*',null,null],
            [ '销售价维护(新增)','skuPrice.create','POST','/skuPrices',null,null],


            [ '供应商管理','supplier.list','GET','/suppliers',null,null],
            [ '供应商管理(编辑页面)','supplier.edit.html','GET','/suppliers/*/edit',null,null],
            [ '供应商管理(新增页面)','supplier.create.html','GET','/suppliers/create',null,null],
            [ '供应商管理(编辑)','supplier.edit','PUT','/suppliers/*',null,null],
            [ '供应商管理(删除)','supplier.delete','DELETE','/suppliers/*',null,null],
            [ '供应商管理(新增)','supplier.create','POST','/suppliers',null,null],

            [ '采购价维护','supplierGood.list','GET','/supplierGoods',null,null],
            [ '采购价维护(编辑页面)','supplierGood.edit.html','GET','/supplierGoods/*/edit',null,null],
            [ '采购价维护(新增页面)','supplierGood.create.html','GET','/supplierGoods/create',null,null],
            //[ '采购价维护(编辑)','supplierGood.edit','PUT','/supplierGoods',null,null],
            [ '采购价维护(删除)','supplierGood.delete','DELETE','/supplierGoods/*',null,null],
            [ '采购价维护(新增)','supplierGood.create','POST','/supplierGoods',null,null],
            [ '采购价维护(财务审核)','supplierGood.auditing','POST','/supplierGoods/mySubmit',null,null],

            [ '销售管理','order.list','GET','/orders',null,null],
            [ '销售管理(编辑页面)','order.edit.html','GET','/orders/*/edit',null,null],
            [ '销售管理(新增页面)','order.create.html','GET','/orders/create',null,null],
            //[ '销售管理(编辑)','order.edit','PUT','/orders',null,null],
            [ '销售管理(删除)','order.delete','DELETE','/orders/*',null,null],
            [ '销售管理(新增)','order.create','POST','/orders',null,null],
            [ '销售管理(财务审核)','order.auditing','POST','/orders/*/finance',null,null],
            [ '销售管理(销售详情)','order.particular','GET','/orders/*',null,null],

            [ '采购管理','purchase.list','GET','/purchases',null,null],
            [ '采购管理(编辑页面)','purchase.edit.html','GET','/purchases/*/edit',null,null],
            [ '采购管理(新增页面)','purchase.create.html','GET','/purchases/create',null,null],
            [ '采购管理(编辑)','purchase.edit','PUT','/purchases/*',null,null],
            [ '采购管理(删除)','purchase.delete','DELETE','/purchases/*',null,null],
            [ '采购管理(新增)','purchase.create','POST','/purchases',null,null],
            [ '采购管理(财务审核)','purchase.auditing','POST','/purchases/*/finance',null,null],
            [ '采购管理(采购详情)','purchase.particular','GET','/purchases/*',null,null],

            [ '仓库管理','warehouse.list','GET','/warehouses',null,null],
            [ '仓库管理(编辑页面)','warehouse.edit.html','GET','/warehouses/*/edit',null,null],
            [ '仓库管理(新增页面)','warehouse.create.html','GET','/warehouses/create',null,null],
            [ '仓库管理(编辑)','warehouse.edit','PUT','/warehouses/*',null,null],
            [ '仓库管理(删除)','warehouse.delete','DELETE','/warehouses/*',null,null],
            [ '仓库管理(新增)','warehouse.create','POST','/warehouses',null,null],

            [ '库区管理','warehouseszone.list','GET','/warehouseszones',null,null],
            [ '库区管理(编辑页面)','warehouseszone.edit.html','GET','/warehouseszones/*/edit',null,null],
            [ '库区管理(新增页面)','warehouseszone.create.html','GET','/warehouseszones/create',null,null],
            [ '库区管理(编辑)','warehouseszone.edit','PUT','/warehouseszones/*',null,null],
            [ '库区管理(删除)','warehouseszone.delete','DELETE','/warehouseszones/*',null,null],
            [ '库区管理(新增)','warehouseszone.create','POST','/warehouseszones',null,null],

            [ '库位管理','warehouselocation.list','GET','/warehouselocations',null,null],
            [ '库位管理(编辑页面)','warehouselocation.edit.html','GET','/warehouselocations/*/edit',null,null],
            [ '库位管理(新增页面)','warehouselocation.create.html','GET','/warehouselocations/create',null,null],
            [ '库位管理(编辑)','warehouselocation.edit','PUT','/warehouselocations/*',null,null],
            [ '库位管理(删除)','warehouselocation.delete','DELETE','/warehouselocations/*',null,null],
            [ '库位管理(新增)','warehouselocation.create','POST','/warehouselocations',null,null],

            [ '库存管理','inventory.list','GET','/inventorys',null,null],
            [ '库存管理(编辑页面)','inventory.edit.html','GET','/inventorys/*/edit',null,null],
            [ '库存管理(新增页面)','inventory.create.html','GET','/inventorys/create',null,null],
            [ '库存管理(编辑)','inventory.edit','PUT','/inventorys/*',null,null],
            [ '库存管理(删除)','inventory.delete','DELETE','/inventorys/*',null,null],
            [ '库存管理(新增)','inventory.create','POST','/inventorys',null,null],

            [ '库存操作','inventoryCheckingDetail.list','GET','/inventoryCheckingDetails',null,null],
            [ '库存操作(编辑页面)','inventoryCheckingDetail.edit.html','GET','/inventoryCheckingDetails/*/edit',null,null],
            [ '库存操作(新增页面)','inventoryCheckingDetail.create.html','GET','/inventoryCheckingDetails/create',null,null],
            [ '库存操作(编辑)','inventoryCheckingDetail.edit','PUT','/inventoryCheckingDetails/*',null,null],
            [ '库存操作(删除)','inventoryCheckingDetail.delete','DELETE','/inventoryCheckingDetails/*',null,null],
            [ '库存操作(新增)','inventoryCheckingDetail.create','POST','/inventoryCheckingDetails',null,null],

            [ '入库管理','in.list','GET','/ins',null,null],
            [ '入库管理(编辑页面)','in.edit.html','GET','/ins/*/edit',null,null],
            [ '入库管理(新增页面)','in.create.html','GET','/ins/create',null,null],
            //[ '入库管理(编辑)','in.edit','PUT','/ins',null,null],
            [ '入库管理(删除)','in.delete','DELETE','/ins/*',null,null],
            [ '入库管理(新增)','in.create','POST','/ins',null,null],
            [ '入库管理(入库操作)','in.warehouse','POST','/ins/*/finance',null,null],
            [ '入库管理(入库详情)','in.particular','GET','/ins/*',null,null],

            [ '出库管理','out.list','GET','/outs',null,null],
            [ '出库管理(编辑页面)','out.edit.html','GET','/outs/*/edit',null,null],
            [ '出库管理(新增页面)','out.create.html','GET','/outs/create',null,null],
            //[ '出库管理(编辑)','out.edit','PUT','/outs',null,null],
            [ '出库管理(删除)','out.delete','DELETE','/outs/*',null,null],
            [ '出库管理(新增)','out.create','POST','/outs',null,null],
            [ '出库管理(出库操作)','out.warehouse','POST','/outs/*/finance',null,null],
            [ '出库管理(出库详情)','out.particular','GET','/outs/*',null,null],


            [ '销售主管审核','order.charge','POST','/orders/*/auditing',null,null],
            [ '采购主管审核','purchase.charge','POST','/purchases/*/auditing',null,null],
            [ '调拨单','transfer_bill.list','GET','/transferBill',null,null],
            [ '调拨单(新增页面)','transfer_bill.create.html','GET','/transferBill/create',null,null],
            [ '调拨单(新增)','transfer_bill.create','POST','/transferBill',null,null],
            [ '调拨单(详情)','transfer_bill.particular','GET','/transferBill/*',null,null],


            [ '调拨入库','transfer_in.list','GET','/transferIns',null,null],
            // [ '调拨入库(新增页面)','transfer_in.create.html','GET','/transferBIns/create',null,null],
            // [ '调拨入库（新增）','transfer_in.create','POST','/transferBIns',null,null],
            [ '调拨入库(调拨入库详情)','transfer_in.particular','GET','/transferIns/*',null,null],
            [ '调拨入库(调拨入库操作)','transfer_in.warehouse','POST','/transferIns/*/finance',null,null],



            [ '调拨出库','transfer_out.list','GET','/transferOuts',null,null],
            // [ '调拨出库(新增页面)','transfer_bill.create.html','GET','/transferBill/create',null,null],
            // [ '调拨出库(新增)','transfer_bill.create','POST','/transferBill',null,null],
            [ '调拨出库(调拨出库详情)','transfer_out.particular','GET','/transferOuts/*',null,null],
            [ '调拨出库(调拨出库操作)','transfer_out.warehouse','POST','/transferOuts/*/finance',null,null],


            [ '仓库主管审核','transfer_bill.charge','POST','/transferBill/*/auditing',null,null],

            [ '用户管理', 'auth.users', 'GET', '/auth/users', null, null],
            [ '用户管理(编辑页面)','users.edit.html','GET','/auth/users/*/edit',null,null],
            [ '用户管理(新增页面)','users.create.html','GET','/auth/users/create',null,null],
            [ '用户管理(新增)','users.create','POST','/auth/users',null,null],
            [ '用户管理(编辑)','users.edit','PUT','/auth/users/*',null,null],
            [ '用户管理(删除)','users.delete','DELETE','/auth/users/*',null,null],

            [ '公司管理', 'auth.company', 'GET', '/company', null, null],
            [ '公司管理(编辑页面)','company.edit.html','GET','/company/*/edit',null,null],
            [ '公司管理(新增页面)','company.create.html','GET','/company/create',null,null],
            [ '公司管理(新增)','company.create','POST','/company',null,null],
            [ '公司管理(编辑)','company.edit','PUT','/company/*',null,null],
            [ '公司管理(删除)','company.delete','DELETE','/company/*',null,null],

            [ '采购财务标识全款','purchase.getAll','GET','/purchase/*/getAll',null,null],
            [ '采购财务付款','purchase.pay','POST','/purchase/*/pay',null,null],



        ];
        $admin_permissions = [];
        foreach ($permissions as $permission) {
            $temp_permission = [];
            // $temp_permission['id'] = $permission[0];
            $temp_permission['name'] = $permission[0];
            $temp_permission['slug'] = $permission[1];
            $temp_permission['http_method'] = $permission[2];
            $temp_permission['http_path'] = $permission[3];
            $created_at = $permission[4]?$permission[4]:date('Y-m-d H:i:s');
            $updated_at = $permission[5]?$permission[5]:date('Y-m-d H:i:s');
            $temp_permission['created_at'] = $created_at;
            $temp_permission['updated_at'] = $updated_at;
            array_push($admin_permissions, $temp_permission);
        }
        \DB::table('admin_permissions')->insert($admin_permissions);
    }

    public function down()
    {
        \DB::table('admin_permissions')->truncate();
    }
}
