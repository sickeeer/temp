<?php

use Illuminate\Database\Seeder;

class AdminTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_users')->truncate();
        DB::table('admin_role_permissions')->truncate();
        DB::table('admin_roles')->truncate();
        DB::table('admin_role_users')->truncate();
        DB::table('admin_menu')->truncate();

        // admin_users 创建初始用户
        DB::table('admin_users')->insert([
            [
                'username' =>'admin',
                'name' =>'塞卡',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '123456789',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

            [
                'username' =>'gsadmin',
                'name' =>'公司管理账号',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '123456789',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

            [
                'username' =>'cwadmin',
                'name' =>'财务管理账号',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '123456789',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

            [
                'username' =>'xsadmin',
                'name' =>'销售管理账号',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '123456789',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

            [
                'username' =>'cgadmin',
                'name' =>'采购管理账号',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '13601868340',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

            [
                'username' =>'ckadmin',
                'name' =>'仓库管理账号',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '123456789',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

            [
                'username' =>'xszgadmin',
                'name' =>'销售主管账号',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '123456789',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

            [
                'username' =>'cgzgadmin',
                'name' =>'采购主管账号',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '123456789',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

            [
                'username' =>'ckzgadmin',
                'name' =>'仓库主管账号',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '123456789',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],

            [
                'username' =>'cgjeadmin',
                'name' =>'采购金额账号',
                'email' =>'123456789@gmail.com',
                'password' => bcrypt('123456789'),
                'company_id' => 1,
                'tel' => '123456789',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);

        // admin_role_permissions 为第一个用户组（超级管理 配权 初始全权限 *）
        DB::table('admin_role_permissions')
            ->insert([
                'role_id'=>1,
                'permission_id'=>1,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);

        // admin_roles 配置角色表
        DB::table('admin_roles')
            ->insert([
                [
                'name'=>'超级管理',
                'slug'=>'Administrator',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],

                [
                'name'=>'公司管理',
                'slug'=>'Corporation Management',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],

                [
                'name'=>'财务管理',
                'slug'=>'Finance Management',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],

                [
                'name'=>'销售管理',
                'slug'=>'Sale Management',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],

                // [
                // 'name'=>'跟单',
                // 'slug'=>'Documentary Management',
                // 'created_at'=>date('Y-m-d H:i:s'),
                // 'updated_at'=>date('Y-m-d H:i:s'),
                // ],

                [
                'name'=>'采购管理',
                'slug'=>'Purchase Management',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],

                [
                'name'=>'仓库管理',
                'slug'=>'Warehouse Management',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],

                [
                'name'=>'销售主管',
                'slug'=>'Sale Charge',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],

                [
                'name'=>'采购主管',
                'slug'=>'Purchase Charge',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],

                [
                'name'=>'仓库主管',
                'slug'=>'Warehosue Charge',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],

                [
                'name'=>'采购金额',
                'slug'=>'Purchase Amount',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                ],
            ]);

        // admin_role_users 用户对应角色Id表 为初始用户添加角色
        DB::table('admin_role_users')
            ->insert([
                [
                    'role_id'=>1,
                    'user_id'=>1,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],
                [
                    'role_id'=>2,
                    'user_id'=>2,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],
                [
                    'role_id'=>3,
                    'user_id'=>3,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],
                [
                    'role_id'=>4,
                    'user_id'=>4,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],
                [
                    'role_id'=>5,
                    'user_id'=>5,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],
                [
                    'role_id'=>6,
                    'user_id'=>6,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],
                [
                    'role_id'=>7,
                    'user_id'=>7,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],
                [
                    'role_id'=>8,
                    'user_id'=>8,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],

                [
                    'role_id'=>9,
                    'user_id'=>9,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],

                [
                    'role_id'=>10,
                    'user_id'=>10,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ],

            ]);

        // admin_menu 创建管理菜单
        $menus= [
            ['0', '1', '仪表盘', 'fa-bar-chart', '/', null, null],
            ['0', '2', '用户', 'fa fa-male', '', null, null],
            ['2', '3', '用户管理', 'fa-users', 'auth/users', null, null],
            ['2', '4', '角色管理', 'fa-user', 'auth/roles', null, null],
            ['2', '5', '权限管理', 'fa-ban', 'auth/permissions', null, null],
            ['2', '6', '目录管理', 'fa-bars', 'auth/menu', null, null],
            ['2', '7', '操作日志', 'fa-history', 'auth/logs', null, null],

            ['0', '8', '商品','fa-cubes','',null,null],
            ['8', '9', '商品列表','fa-bars','skus',null,null],
            ['8', '10', '商品分类','fa-bars','categories',null,null],
            ['8', '11', '批次属性维护','fa-bars','batchattributes',null,null],

            ['0', '12', '客户','fa-user-plus','',null,null],
            ['12', '13', '客户管理','fa-bars','agents',null,null],
            ['12', '14', '销售价维护','fa-bars','skuPrices',null,null],

            ['0', '15', '供应商','fa-user-md','',null,null],
            ['15', '16', '供应商管理','fa-bars','suppliers',null,null],
            ['15', '17', '采购价维护','fa-bars','supplierGoods',null,null],

            ['0', '18', '销售单','fa-fax','',null,null],
            ['18', '19', '销售管理','fa-bars','orders',null,null],

            ['0', '20', '采购单','fa-truck','',null,null],
            ['20', '21', '采购管理','fa-bars','purchases',null,null],

            ['0', '22', '仓库','fa-university','',null,null],
            ['22', '23', '仓库管理','fa-bars','warehouses',null,null],
            ['22', '24', '库区管理','fa-bars','warehouseszones',null,null],
            ['22', '25', '库位管理','fa-bars','warehouselocations',null,null],

            ['0', '26', '库存','fa-shopping-basket','',null,null],
            ['26', '27', '库存管理','fa-bars','inventorys',null,null],
            ['26', '28', '库存操作','fa-bars','inventoryCheckingDetails',null,null],

            ['0', '29', '入库','fa-toggle-down','',null,null],
            ['29', '30', '入库管理','fa-bars','ins',null,null],
            //['29', '31', '入库单据','fa-bars','inBills',null,null],

            ['0', '31', '出库','fa-toggle-up','',null,null],
            ['31', '32', '出库管理','fa-bars','outs',null,null],
            //['32', '34', '出库单据','fa-bars','outBills',null,null],
            ['0', '33', '调拨','fa-arrows','',null,null],
            ['33', '34', '调拨单','fa-bars','transferBill',null,null],
            ['33', '35', '调拨出库','fa-bars','transferOuts',null,null],
            ['33', '36', '调拨入库','fa-bars','transferIns',null,null],

            ['0', '37', '公司','fa-home','',null,null],
            ['37', '38', '公司管理','fa-bars','company',null,null],

        ];
        $admin_menus = [];
        foreach ($menus as $menu) {
            $temp_menumenu = [];
            $temp_menu['parent_id'] = $menu[0];
            $temp_menu['order'] = $menu[1];
            $temp_menu['title'] = $menu[2];
            $temp_menu['icon'] = $menu[3];
            $temp_menu['uri'] = $menu[4];
            $created_at = $menu[5]?$menu[5]:date('Y-m-d H:i:s');
            $updated_at = $menu[6]?$menu[6]:date('Y-m-d H:i:s');
            $temp_menu['created_at'] = $created_at;
            $temp_menu['updated_at'] = $updated_at;
            array_push($admin_menus, $temp_menu);
        }
        DB::table('admin_menu')->insert($admin_menus);
    }

    public function down()
    {
        DB::table('admin_users')->truncate();
        DB::table('admin_role_permissions')->truncate();
        DB::table('admin_roles')->truncate();
        DB::table('admin_role_users')->truncate();
        DB::table('admin_menu')->truncate();
    }
}
