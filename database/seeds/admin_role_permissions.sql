/*
Navicat MySQL Data Transfer

Source Server         : vagrant
Source Server Version : 50719
Source Host           : 192.168.10.10:3306
Source Database       : admin

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-05-24 14:43:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin_role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_permissions`;
CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_role_permissions
-- ----------------------------
INSERT INTO `admin_role_permissions` VALUES ('1', '1', '2018-05-07 19:47:40', '2018-05-07 19:47:40');
INSERT INTO `admin_role_permissions` VALUES ('1', '1', '2018-05-07 15:33:11', '2018-05-07 15:33:11');
INSERT INTO `admin_role_permissions` VALUES ('1', '1', '2018-04-30 12:10:40', '2018-04-30 12:10:40');
INSERT INTO `admin_role_permissions` VALUES ('2', '1', '2018-04-28 06:54:55', '2018-04-28 06:54:55');
INSERT INTO `admin_role_permissions` VALUES ('3', '2', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '3', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '4', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '5', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '9', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '15', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '39', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '21', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '27', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '33', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '45', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '50', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '51', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '56', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '57', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '58', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '64', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '65', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '96', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '103', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '2', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '3', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '4', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '5', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '9', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '15', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '27', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '33', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '39', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '51', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '52', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '53', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '54', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '55', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '57', null, null);
INSERT INTO `admin_role_permissions` VALUES ('4', '21', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '2', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '3', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '4', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '9', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '15', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '21', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '27', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '39', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '45', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '58', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '59', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '60', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '61', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '62', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '63', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '65', null, null);
INSERT INTO `admin_role_permissions` VALUES ('5', '5', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '2', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '3', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '4', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '5', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '9', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '15', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '21', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '27', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '33', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '39', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '45', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '84', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '85', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '86', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '87', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '88', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '89', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '90', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '91', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '92', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '93', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '94', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '95', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '96', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '97', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '98', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '99', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '100', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '101', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '102', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '103', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '104', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '105', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '106', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '107', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '108', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '109', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '84', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '90', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '66', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '67', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '68', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '69', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '70', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '71', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '72', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '73', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '74', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '75', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '76', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '77', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '78', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '79', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '80', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '81', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '82', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '83', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '2', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '3', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '4', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '5', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '9', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '15', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '21', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '27', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '39', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '51', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '52', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '53', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '54', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '55', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '57', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '110', null, null);
INSERT INTO `admin_role_permissions` VALUES ('7', '33', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '2', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '3', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '4', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '5', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '9', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '15', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '21', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '27', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '39', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '45', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '58', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '59', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '60', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '61', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '62', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '63', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '111', null, null);
INSERT INTO `admin_role_permissions` VALUES ('8', '65', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '112', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '113', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '114', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '115', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '116', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '117', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '118', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '119', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '120', null, null);
INSERT INTO `admin_role_permissions` VALUES ('6', '121', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '3', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '4', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '5', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '6', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '15', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '21', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '27', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '33', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '39', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '45', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '66', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '67', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '68', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '69', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '70', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '71', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '72', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '73', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '74', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '75', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '76', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '77', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '78', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '79', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '80', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '81', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '82', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '83', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '84', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '85', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '86', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '87', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '88', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '89', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '90', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '91', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '92', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '93', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '94', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '95', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '96', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '97', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '98', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '99', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '100', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '101', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '102', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '103', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '104', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '105', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '106', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '107', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '108', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '109', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '112', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '113', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '114', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '115', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '116', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '117', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '118', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '119', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '120', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '121', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '122', null, null);
INSERT INTO `admin_role_permissions` VALUES ('9', '2', null, null);
