<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentSellPriceBillDetailTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_sell_price_bill_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku_id')->comment('单品编号');
            $table->string('agent_sell_price_bill_id')->comment('调价单编号');
            $table->string('datam_id')->nullable()->comment('数据列编号');
            $table->string('datam')->nullable()->comment('数据列冗余列');
            $table->tinyInteger('datam_type')->comment('数据类型[0-等级，1-客户]');
            $table->decimal('price',15,4)->comment('价格');
            $table->tinyInteger('enable')->comment('是否有效（0-是，1-否）');
            $table->string('remark')->nullable()->comment('调价理由');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_sell_price_bill_detail');
    }
}
