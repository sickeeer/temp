<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPurchaseDetailColumnContractId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->string('original_id')->nullable()->comment('原始单号');
            $table->string('cowhide_id')->nullable()->comment('皮源编号');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_detail', function (Blueprint $table) {
            $table->dropColumn('original_id');
            $table->dropColumn('cowhide_id');
        });
    }
}
