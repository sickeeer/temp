<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class createIdModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('id_model', function (Blueprint $table) {
            $table->string('common_key',100)->comment('设置键');
            $table->primary('common_key');
            $table->string('common_des')->nullable()->comment('设置描述');
            $table->integer('common_value')->nullable()->comment('设置值');
            $table->string('frequency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('id_model');
    }
}
