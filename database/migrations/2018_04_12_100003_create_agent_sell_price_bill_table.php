<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentSellPriceBillTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_sell_price_bill', function (Blueprint $table) {
        		$table->string('id',100)->comment('调价单编号');
                $table->primary('id');
        		$table->datetime('start_time')->nullable()->comment('开始时间');
        		$table->datetime('end_time')->nullable()->comment('结束时间');
        		$table->string('operater_id')->comment('操作人');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_sell_price_bill');
    }
}
