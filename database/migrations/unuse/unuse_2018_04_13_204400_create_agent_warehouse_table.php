<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agentwarehouse', function (Blueprint $table) {
            $table->increments('warehouse_id');
            $table->string('agentBaseInfo_id')->comment('客户编号');
            $table->string('country')->comment('客户仓库地址(国家)');
            $table->string('province')->comment('客户仓库地址(城市)');
            $table->string('addressInfo')->comment('客户仓库的详细地址');
            $table->string('consignee')->comment('联系人');
            $table->string('phone')->comment('电话号码');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agentwarehouse');
    }
}
