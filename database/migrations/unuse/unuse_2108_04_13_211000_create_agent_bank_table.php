<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_bank', function (Blueprint $table) {
        	$table->increments('agentBank_id');
        		$table->string('agentBaseInfo_id')->comment('客户编号');
        		$table->string('bankName')->comment('银行名称');
        		$table->string('bankAddress')->comment('开户行');
        		$table->string('accountName')->comment('账户名称');
        		$table->string('account')->comment('账号');
        		$table->string('fpPhone')->comment('发票电话');
        		$table->string('fpArea')->nullable()->comment('发票地址');
        		$table->string('taxpayer_id')->comment('纳税人识别号');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_bank');
    }
}
