<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseSupplierTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchasesupplier', function (Blueprint $table) {
            $table->increments('pusu_id');
            $table->string('purchase_id')->comment('采购员');
            $table->string('supplier_id')->comment('供应商');
            $table->string('org_id')->comment('采购员顶级组织编号');
            $table->string('remark')->nullable()->comment('备注');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasesupplier');
    }
}
