<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierBankTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplierbank', function (Blueprint $table) {
            $table->increments('supplierBank_id');
            $table->integer('supplier_id')->comment('供应商编号');
            $table->string('bankName')->comment('银行名称');
            $table->string('bankType')->comment('开户行');
            $table->string('bankAccount')->comment('银行账号');
            $table->string('accountName')->comment('账户名称');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplierbank');
    }
}
