<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierGoodsPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_goods_price_bill_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('supplier_goods_id')->comment('商品sku编码');
            $table->float('min_num')->comment('最小数量');
            $table->float('max_num')->comment('最大数量');
            $table->decimal('price',15,4)->comment('采购价格');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_goods_price_bill_detail');
    }
}
