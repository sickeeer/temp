<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('company', function (Blueprint $table) {
            $table->increments('id')->comment('公司编号');
            $table->string('name')->comment('公司名称');
            $table->string('country')->nullable()->comment('国家');
            $table->string('province')->nullable()->comment('省');
            $table->string('address')->comment('详细地址');
            $table->string('postcode')->nullable()->comment('邮编');
            $table->tinyInteger('status')->default(0)->comment('公司状态（0-正常，1-封存）');
            $table->string('tel')->nullable()->comment('公司联系方式');
            $table->string('linkman')->nullable()->comment('联系人');
            $table->string('bankinfo')->nullable()->comment('开户银行');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
