<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('商品分类名称');
            $table->integer('order')->nullable()->comment('类别排序');
            $table->integer('parent_id')->nullable()->comment('类别父级编号');
            $table->string('remark')->nullable()->comment('栏目说明');
            $table->string('batchattribute')->comment('批次属性');
            $table->tinyinteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
