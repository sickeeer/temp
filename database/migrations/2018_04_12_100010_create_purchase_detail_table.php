<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('purchase_id')->comment('采购单Id');
            $table->string('sku_id')->comment('商品skuId');
            $table->decimal('price',15,4)->comment('商品单价');
            $table->integer('num')->nullable()->comment('规则计数数量');
            $table->float('size',10,2)->nullable()->comment('不规则计数数量');
            $table->tinyInteger('grade')->nullable()->comment('商品等级');
            $table->tinyInteger('thick')->nullable()->comment('商品厚度');
            $table->tinyInteger('origin')->nullable()->comment('商品产地');
            $table->tinyInteger('colour')->nullable()->comment('商品颜色');
            $table->tinyInteger('avg_size')->nullable();
            $table->tinyInteger('min_size')->nullable();

            $table->string('in_state')->default("CREATED")->comment('采购单状态 (CREATED -已生成，CHARGE_CHECK -已提交，PART_IN -部分入库，ALL_IN -已入库，INACTIVED -已取消)');

            $table->string('supplier_product_id')->nullable()->comment('供应商商品编号');
            // $table->integer('arrive_num')->default(0)->comment('到货数量');
            $table->string('arrive_time')->nullable()->comment('预计到货时间');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_detail');
    }
}
