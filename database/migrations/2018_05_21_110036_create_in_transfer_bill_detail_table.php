<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInTransferBillDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('in_transfer_bill_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transfer_bill_id')->comment('订单编号');
            $table->string('sku_id')->comment('单品id');
            $table->string('inventory_id')->comment('批次id');

            $table->integer('num')->nullable()->comment('商品张数');
            $table->float('size',9,4)->nullable()->comment('商品尺寸');

            $table->string('in_state')->default("CREATED");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('in_transfer_bill_detail');
    }
}
