<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInventoryCheckingDetialColumnInventorySn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_checking_detail', function (Blueprint $table) {
            $table->integer('inventory_sn')->nullable()->comment('批次序号');
            $table->tinyInteger('grade_o')->nullable();
            $table->tinyInteger('thick_o')->nullable();
            $table->tinyInteger('origin_o')->nullable();
            $table->tinyInteger('colour_o')->nullable();
            $table->tinyInteger('avg_size_o')->nullable();
            $table->tinyInteger('min_size_o')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_checking_detail', function (Blueprint $table) {
            $table->dropColumn('inventory_sn');
            $table->dropColumn('grade_o');
            $table->dropColumn('thick_o');
            $table->dropColumn('origin_o');
            $table->dropColumn('colour_o');
            $table->dropColumn('avg_size_o');
            $table->dropColumn('min_size_o');
        });
    }
}
