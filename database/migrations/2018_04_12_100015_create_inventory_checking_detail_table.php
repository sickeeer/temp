<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryCheckingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_checking_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku_id')->comment('sku编号');
            $table->tinyInteger('type')->comment('1 出库 2入库  3出库锁定 4入库锁定');
            $table->integer('num')->nullable()->comment('涉及数量');
            $table->decimal('size',15,4)->nullable()->comment('涉及尺数');

            $table->string('operater_id')->comment('操作人');
            $table->string('checking_ASN')->nullable()->comment('单据');
            $table->string('inventory_id')->nullable()->comment('库存编号');
            $table->tinyInteger('grade')->nullable()->comment('商品等级');
            $table->tinyInteger('thick')->nullable()->comment('商品厚度');
            $table->tinyInteger('origin')->nullable()->comment('商品产地');
            $table->tinyInteger('colour')->nullable()->comment('商品颜色');
            $table->tinyInteger('avg_size')->nullable();
            $table->tinyInteger('min_size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_checking_detail');
    }
}
