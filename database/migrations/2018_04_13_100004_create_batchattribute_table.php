<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchattributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batchattribute', function (Blueprint $table) {
            $table->increments('id',100);
            $table->string('parent_id')->comment('批次编号');
            $table->string('name')->comment('批次名称');
            $table->string('value',100)->comment('批次值')->unique();
            $table->tinyinteger('status')->default(0)->comment('是否删除');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batchattribute');
    }
}
