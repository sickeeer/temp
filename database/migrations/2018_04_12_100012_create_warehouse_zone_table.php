<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseZoneTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_zone', function (Blueprint $table) {
        		$table->string('id',100)->comment('库区编号');
                $table->primary('id');
        		$table->string('name')->comment('库区名称');
        		$table->tinyInteger('status')->default(0)->comment('');
        		$table->string('remark')->nullable()->comment('');
        		$table->string('warehouse_id')->nullable()->comment('仓库编号');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_zone');
    }
}
