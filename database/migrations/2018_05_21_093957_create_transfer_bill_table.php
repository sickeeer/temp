<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_bill', function (Blueprint $table) {
            $table->string('id',100)->comment('订单id');
            $table->primary('id');
            $table->string('out_warehouse_id')->comment('调出仓库');
            $table->string('in_warehouse_id')->comment('调入仓库');
            $table->string('transfer_bill_out_state')->default("CREATED");
            $table->string('transfer_bill_in_state')->default("CREATED");

            // $table->string('documentary')->nullable()->comment('跟单人员');
            $table->tinyInteger('logistics')->comment('物流方式 自提 物流');
            $table->string('remark')->nullable()->comment('备注');
            $table->datetime('send_time')->comment('出库时间');
            $table->string('review_in_state')->default('CREATED');
            $table->string('review_out_state')->default('CREATED');

            $table->string('reviewer_id')->nullable()->comment('审核人编号');
            $table->datetime('review_time')->nullable()->comment('审核时间');
            $table->string('operater_id')->comment('操作人编号');
            $table->string('company_id')->nullable()->comment('公司id');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_bill');
    }
}
