<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id')->comment('订单编号');
            $table->string('sku_id')->comment('单品id');
            $table->integer('num')->nullable()->comment('商品张数');
            $table->float('size',9,4)->nullable()->comment('商品尺寸');
            // $table->float('arrive_num',9,4)->default(0)->comment('到货商品尺寸');
            $table->tinyInteger('grade')->nullable()->comment('商品等级');
            $table->tinyInteger('thick')->nullable()->comment('商品厚度');
            $table->tinyInteger('origin')->nullable()->comment('商品产地');
            $table->tinyInteger('colour')->nullable()->comment('商品颜色');
            $table->tinyInteger('avg_size')->nullable();
            $table->tinyInteger('min_size')->nullable();
            $table->string('arrive_sku_id')->nullable()->comment('外协入库sku');
            $table->decimal('price',15,4)->comment('商品单价');
            $table->string('out_state')->default("CREATED")->comment('出库状态 (CREATED -已生成，PART_OUT -部分出库，ALL_OUT -已出库 )');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_detail');
    }
}
