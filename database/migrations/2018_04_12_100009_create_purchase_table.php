<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase', function (Blueprint $table) {
                $table->string('id',100)->comment('采购单编号');
                $table->primary('id');
                $table->integer('supplier_id')->comment('供应商编号');
                $table->integer('company_id')->comment('采购/操作人公司Id');
                $table->string('operater_id')->comment('操作人ID/采购员');
                $table->string('purchase_state')->default("CREATED")->comment('采购单状态 (CREATED -已生成，CHARGE_CHECK -已提交，PART_IN -部分入库，ALL_IN -已入库，INACTIVED -已取消)');
                $table->decimal('total_amount',15,4)->comment('含税金额');
                $table->float('rate',5,2)->comment('税率');
                $table->decimal('tax',15,4)->comment('税额');
                $table->float('exchange_rate',5,2)->comment('汇率');
                $table->decimal('exchange_amount',15,4)->comment('汇率后含税价');
                // $table->datetime('send_time')->nullable()->comment('到货时间');
                $table->string('bar_code')->nullable()->comment('条形码url');
                $table->string('warehouse_id')->comment('仓库Id');
                $table->tinyInteger('pay_type')->nullable()->comment('付款方式 现金 电汇 支票');
                $table->tinyInteger('payment_state')->default(0)->comment('付款状态 0 未付 1 部分 2 全款');
                $table->integer('paid_amount')->default(0)->comment('已付金额');
                $table->tinyInteger('type')->comment('采购单类型单类型：0 公司间采购 1 供应商采购');
                $table->string('review_state')->default('CREATED')->comment('CREATED 未审核 CHARGE_CHECK 主管审核 FINANCE_1ST 财务1审 IN_CHECK 入库审核 FINANCE_LAST 财务2审  INACTIVED 取消');
                $table->string('reviewer_id')->nullable()->comment('最后审核人');
                $table->datetime('review_time')->nullable()->comment('最后审核状态');
                $table->string('remark')->nullable()->comment('备注');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase');
    }
}
