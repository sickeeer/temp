<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('客户名称');
            $table->string('type')->comment('类型(0:供应商,1:公司供应商)');
            $table->string('country')->nullable()->comment('国家');
            $table->string('province')->nullable()->comment('城市');
            $table->string('address')->comment('地址');
            $table->string('telephone')->nullable()->comment('固定电话');
            $table->string('phone')->nullable()->comment('手机');
            $table->string('fax')->nullable()->comment('传真');
            $table->string('email')->nullable()->comment('邮箱');
            $table->tinyInteger('pay_type')->nullable()->comment('付款方式(0:现金,1:电汇,2:支票)');
            $table->integer('postCode')->nullable()->comment('邮编');
            $table->tinyInteger('currency')->comment('币种');
            $table->string('remark')->nullable()->comment('备注');
            $table->string('rate')->comment('进项税');
            $table->string('salesman')->nullable()->comment('销售');
            $table->string('merchandiser')->nullable()->comment('跟单');
            $table->tinyInteger('status')->default(0)->comment('状态(0:可用,1:禁用)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier');
    }
}
