<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
                $table->increments('id')->comment('自增列');
                $table->string('inventory_id')->comment('批次编号');
                // $table->string('lockedCnt')->comment('锁定库存');
                $table->string('over_num')->comment('可用库存');
                $table->string('over_size')->comment('可用库存');
                $table->decimal('price',15,4)->comment('入库单价');
                $table->string('sku_id')->comment('sku编码');
                $table->string('warehouse_zone_id')->comment('库位编码');
                $table->string('warehouse_location_id')->comment('位置编码');
                $table->string('warehouse_id')->comment('仓库编码');
                $table->tinyInteger('grade')->nullable()->comment('商品等级');
                $table->tinyInteger('thick')->nullable()->comment('商品厚度');
                $table->tinyInteger('origin')->nullable()->comment('商品产地');
                $table->tinyInteger('colour')->nullable()->comment('商品颜色');
                $table->tinyInteger('avg_size')->nullable();
                $table->tinyInteger('min_size')->nullable();
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
