<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutTransferBillDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('out_transfer_bill_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transfer_bill_id')->comment('订单编号');

            $table->string('sku_id')->comment('单品id');
            $table->string('inventory_id')->comment('批次id');

            $table->integer('num')->nullable()->comment('商品张数');
            $table->float('size',9,4)->nullable()->comment('商品尺寸');
            $table->string('out_state')->default("CREATED")->comment('出库状态 (CREATED -已生成，PART_OUT -部分出库，ALL_OUT -已出库 )');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('out_transfer_bill_detail');
    }
}
