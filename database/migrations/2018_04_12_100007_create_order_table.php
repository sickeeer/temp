<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->string('id',100)->comment('订单id');
            $table->primary('id');
            $table->tinyInteger('type')->comment('订单类型(0:销售单,1:外协出库单)');
            $table->decimal('total_price',15,4)->comment('订单总价');
            $table->string('agent_id')->comment('客户Id');
            // $table->string('salesman')->comment('销售 agent对应salesman');
            $table->string('order_state')->default("CREATED")->comment('销售单状态 (CREATED -已生成，CHARGE_CHECK -已提交，PART_OUT -部分出库，ALL_OUT -已出库，INACTIVED -已取消)');
            // $table->string('documentary')->nullable()->comment('跟单人员');
            $table->tinyInteger('logistics')->comment('物流方式 自提 物流');
            $table->tinyInteger('pay_type')->comment('支付方式 现金 电汇 支票');
            $table->string('payment_state')->default(0)->comment('支付状态 未付款 部分 全款');
            $table->decimal('actual_money',15,4)->nullable()->comment('实付金额');
            $table->tinyInteger('invoice')->nullable()->comment('是否开发票');
            $table->decimal('invoice_money',11,4)->nullable()->comment('发票费用');
            $table->string('remark')->nullable()->comment('备注');
            $table->datetime('send_time')->comment('出库时间');
            $table->float('rate',5,2)->comment('税率');
            $table->decimal('tax',15,4)->comment('税额');
            $table->datetime('arrive_time')->nullable()->comment('外协入库时间');
            $table->string('review_state')->default('CREATED')->comment('CREATED 未审核 CHARGE_CHECK 主管审核 FINANCE_1ST 财务1审 IN_CHECK 入库审核 FINANCE_LAST 财务2审  INACTIVED 取消');

            $table->string('is_send')->comment('是否有货先发（1：是，2：否）');
            $table->string('company_id')->nullable()->comment('公司id');
            $table->decimal('pay_money',15,4)->default(0)->comment('已付款金额');
            $table->string('reviewer_id')->nullable()->comment('审核人编号');
            $table->datetime('review_time')->nullable()->comment('审核时间');
            $table->string('operater_id')->comment('操作人编号');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
