<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse', function (Blueprint $table) {
        		$table->string('id',100)->comment('仓库编号');
                $table->primary('id');
        		$table->integer('company_id')->comment('公司编号');
        		$table->string('name')->comment('仓库名称');
        		$table->string('province')->nullable()->comment('城市');
        		$table->string('country')->nullable()->comment('国家');
        		$table->string('address')->comment('详细地址');
        		$table->tinyinteger('status')->default(0)->comment('是否删除');
                $table->string('remark')->nullable()->comment('备注');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse');
    }
}
