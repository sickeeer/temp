<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierGoodsPriceBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_goods_price_bill', function (Blueprint $table) {
            $table->string('id',100);
            $table->primary('id');
            $table->string('sku_id')->comment('skuId');
            $table->string('supplier_id')->comment('供应商Id');
            $table->string('cycle')->comment('采购周期');
            $table->decimal('price',15,4)->comment('价格');
            $table->string('remark')->nullable()->comment('备注');
            $table->tinyInteger('status')->default(0)->comment('商品状态(0:可用,1:禁用)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_goods_price_bill');
    }
}
