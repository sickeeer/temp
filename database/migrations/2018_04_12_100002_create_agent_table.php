<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('客户公司名称');
            $table->string('country')->nullable()->comment('客户地址(国家)');
            $table->string('province')->nullable()->comment('客户地址(城市)');
            $table->string('address')->comment('客户的详细地址');
            $table->string('contacts_person')->nullable()->comment('联系人');
            $table->string('phone')->nullable()->comment('手机号码');
            $table->string('email')->nullable()->comment('邮箱');
            $table->string('faxes')->nullable()->comment('传真');
            $table->decimal('out_tax',8,4)->comment('销项税率');
            $table->string('taxpayer_id')->nullable()->comment('纳税人识别号');
            $table->string('fp_area')->nullable()->comment('发票地址');
            $table->string('fp_phone')->nullable()->comment('发票电话');
            $table->string('customer_source')->nullable()->comment('客户来源');
            $table->string('salesman')->nullable()->comment('销售员');
            $table->string('merchandiser')->nullable()->comment('跟单员');
            // $table->string('logistics_type')->nullable()->comment('物流方式');
            $table->string('remark')->nullable()->comment('备注');
            $table->tinyInteger('status')->default(0)->comment('客户状态（0:生效，1:禁用，2:删除）');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent');
    }
}
