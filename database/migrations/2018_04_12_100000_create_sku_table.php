<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkuTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sku', function (Blueprint $table) {
            $table->string('id',100)->comment('商品编号');
            $table->primary('id');
            $table->string('name')->comment('商品名称');
            $table->integer('category_id')->comment('分类编号');
            $table->string('batchattribute')->nullable()->comment('批次分类');
            $table->string('unit')->nullable()->comment('商品单位');
            $table->tinyInteger('status')->default(0)->comment('0 正常 1冻结 2删除');
            $table->integer('pack_num')->comment('包装数量');
            $table->string('remark')->nullable()->comment('备注');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sku');
    }
}
