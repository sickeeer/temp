<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderDetailColumnContractId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('order_detail', function (Blueprint $table) {
            $table->string('contract_id')->nullable()->comment('合同订单编号');
            $table->string('original_id')->nullable()->comment('原始单号');
            $table->string('cowhide_id')->nullable()->comment('皮源编号');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_detail', function (Blueprint $table) {
            $table->dropColumn('contract_id');
            $table->dropColumn('original_id');
            $table->dropColumn('cowhide_id');
        });
    }
}
