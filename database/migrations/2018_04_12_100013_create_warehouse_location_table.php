<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class  CreateWarehouseLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_location', function (Blueprint $table) {
        		$table->string('id',100)->nullable()->comment('库存位置编号');
                $table->primary('id');
        		$table->string('name')->nullable()->comment('库存位置名字');
        		$table->string('warehouse_zone_id')->nullable()->comment('库区编号');
        		$table->tinyInteger('warehouse_location_type')->nullable()->comment('库存位置类型');
        		$table->string('RFCode')->nullable()->comment('RFCode');
        		$table->tinyInteger('status')->nullable()->comment();
                $table->string('remark')->nullable()->comment('备注');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_location');
    }
}
