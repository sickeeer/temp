<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();
// 新增采购单拉取信息
Route::post('/purchase/ajaxSkuOtherInfos', '\App\Admin\Controllers\PurchaseController@ajaxSkuOtherInfos');
// 新增销售单拉取信息
Route::post('/orders/ajaxSkuOtherInfos', '\App\Admin\Controllers\OrdersController@ajaxSkuOtherInfos');

Route::post('/transferBill/ajaxSkuOtherInfos', '\App\Admin\Controllers\TransferBillController@ajaxSkuOtherInfos');

Route::post('/purchase/supplierInfo', '\App\Admin\Controllers\PurchaseController@supplierInfo');
Route::post('/orders/agentInfo', '\App\Admin\Controllers\OrdersController@agentInfo');

Route::get('/admin/helpers/warehouseLocation','\App\Admin\Controllers\HelpersController@warehouseLocation');
Route::get('/admin/helpers/warehouseZone','\App\Admin\Controllers\HelpersController@warehouseZone');
Route::get('/admin/helpers/inventory_id','\App\Admin\Controllers\HelpersController@inventory_id');

Route::get('/admin/purchases/pdf/{id}','\App\Admin\Controllers\PurchaseController@pdf')->name('purchases.pdf');

Route::post('/admin/outs/batchInfo','\App\Admin\Controllers\OutsController@batchInfo');
Route::get('/admin/agents/create2','\App\Admin\Controllers\AgentController@create2');
Route::post('/admin/sysConfig/setJson','\App\Admin\Controllers\ConfigController@setJson');

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    // 供应商调价 新增
    $router->post('/supplierGoods/mySubmit', 'SupplierGoodsController@mySubmit');

    // 销售单财务审核
    $router->post('/orders/{id}/finance', 'OrdersController@finance');

    $router->post('/orders/{id}/auditing', 'OrdersController@auditing');

    // 采购单财务审核
    $router->post('/purchases/{id}/finance', 'PurchaseController@finance');

    $router->post('/purchases/{id}/pay', 'PurchaseController@pay');

    // 采购单主管审核
    $router->post('/purchases/{id}/auditing', 'PurchaseController@auditing');

    // 采购单财务标识全部付款
    $router->get('/purchases/{id}/setAll', 'PurchaseController@setAll');

    $router->post('/transferBill/{id}/auditing', 'TransferBillController@auditing');

    // 仓库入库操作
    $router->post('/ins/{id}/finance', 'InsController@finance');

    // 仓库出库操作
    $router->post('/outs/{id}/finance', 'OutsController@finance');

    $router->post('/transferIns/{id}/finance', 'TransferInsController@finance');
    $router->post('/transferOuts/{id}/finance', 'TransferOutsController@finance');


    $router->resources([
        /*已完成*/
        'skus'=> SkuController::class,
        'categories'=> CategoryController::class,
        'agents'=> AgentController::class,
        'skuPrices'=>SkuPriceController::class,
        'suppliers'=> SupplierController::class,
        'supplierGoods'=> SupplierGoodsController::class,
        'purchases'=> PurchaseController::class,
        'warehouses'=> WarehouseController::class,
        'warehouseszones'=> WarehousezoneController::class,
        'warehouselocations'=> WarehouselocationsController::class,
        'inventorys'=> InventoryController::class,
        'orders'=> OrdersController::class,
        'inventoryCheckingDetails'=> InventorycheckingdetailController::class,
        'batchattributes'=> BatchattributeController::class,
        'changePriceBills'=>ChangePriceBillController::class,
        'ins'=>InsController::class,
        'outs'=>OutsController::class,
        'transferBill'=>TransferBillController::class,
        'transferIns'=>TransferInsController::class,
        'transferOuts'=>TransferOutsController::class,
        'company'=>CompanyController::class,
        'sysConfig'=>ConfigController::class

        //'inBills'=>InBillsController::class,
        //'outBills'=>OutsController::class,


        /*待定*/
       // 'purchasedetails'=> InController::class,
       // 'orderdetails'=> OrderdetailController::class,




        /*未完成*/

        // 'inventory_details'=> InventorydetailController::class,
        // 'agent_banks'=> Agent_bankController::class,
        // 'supplierGoodsPrices'=> SupplierGoodsPriceController::class,
        // 'supplierbanks'=> SupplierbankController::class,
        // 'purchasesuppliers'=> PurchasesupplierController::class,
        // 'productattributes'=> ProductattributeController::class,
        // 'agentwarehouses'=> AgentwarehouseController::class,
    ]);
});
