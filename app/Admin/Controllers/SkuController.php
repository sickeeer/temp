<?php

namespace App\Admin\Controllers;

use App\Models\Sku;
use App\Models\Category;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Admin\Requests\SkuRequest;

class SkuController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('商品');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('商品');
            $content->description('');
            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('商品');
            $content->description('');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Sku::class, function (Grid $grid) {
            $grid->model()->orderBy('sku.created_at','desc');
            $grid->id('编号')->sortable();
            $grid->name('商品名称');
            $grid->column('category.title','分类编号');
            //$grid->batchattribute('批次分类');
            //$grid->unit('商品单位');

            $grid->pack_num('包装数量');
            $grid->status('商品状态')->display(function($v){
                return HelpersController::getStateValue('state',$v);
            });
            $grid->disableExport();//禁用导出按钮

            $grid->filter(function($filter){
                // 去掉默认的id过滤器
                $filter->disableIdFilter();

                $filter->where(function ($query) {
                    $query->where('sku.id', 'like', "%{$this->input}%");
                }, '编号');

                $filter->where(function ($query) {
                    $query->where('sku.name', 'like', "%{$this->input}%");
                }, '商品名称');

                $filter->where(function ($query) {
                    $query->where('sku.name', 'like', "%{$this->input}%");
                }, '分类编号');
            });
           //$grid->remark('备注');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Sku::class, function ( $form) {
            // if(explode(".",\Route::currentRouteName())[1] == 'create'){

            // }else{
            //     $form->display('sku_id','SKU编号 *');
            // }
            $form->text('id','SKU编号 *')->rules('required|unique:sku',['required' => 'SKU编号必填','unique'=>'SKU已存在']);

            $form->text('name','商品名称 *')->rules('required',['required' => '商品名称必填']);

            // dd($form);

            $form->select('category_id','分类编号 *')->options(Category::selectOptions())->rules('required',['required' => '分类编号必填']);
            $form->multipleSelect('batchattribute','批次分类')->options(json_decode(config('batchAttribute'),true))->help('不填默认为分类中绑定批次属性');
            $form->text('unit','商品单位 *')->rules('required',['required' => '商品单位必填']);
            $form->select('status','商品状态')->options(HelpersController::getStateOptions('state'));
            $form->number('pack_num','包装数量')->rules('regex:/^[1-9]\d*$/',['regex' => '包装数量至少大于1']);
            $form->textarea('remark','备注');
            // $states = [
            //     'on'  => ['value' => 0, 'text' => '激活', 'color' => 'success'],
            //     'off' => ['value' => 1, 'text' => '冻结', 'color' => 'danger'],
            // ];
            // $form->switch('status','商品状态')->states($states);
        });
    }
    protected function editForm()
    {
        return Admin::form(Sku::class, function ( $form) {
            // if(explode(".",\Route::currentRouteName())[1] == 'create'){

            // }else{
            //     $form->display('sku_id','SKU编号 *');
            // }
            $form->display('id','SKU编号 *');

            $form->text('name','商品名称 *')->rules('required',['required' => '商品名称必填']);

            // dd($form);

            $form->select('category_id','分类编号 *')->options(Category::selectOptions())->rules('required',['required' => '分类编号必填']);
            $form->multipleSelect('batchattribute','批次分类')->options(json_decode(config('batchAttribute'),true))->help('不填默认为分类中绑定批次属性');
            $form->text('unit','商品单位 *')->rules('required',['required' => '商品单位必填']);
            $form->select('status','商品状态')->options(HelpersController::getStateOptions('state'));
            $form->number('pack_num','包装数量')->rules('regex:/^[1-9]\d*$/',['regex' => '包装数量至少大于1']);
            $form->textarea('remark','备注');
            // $states = [
            //     'on'  => ['value' => 0, 'text' => '激活', 'color' => 'success'],
            //     'off' => ['value' => 1, 'text' => '冻结', 'color' => 'danger'],
            // ];
            // $form->switch('status','商品状态')->states($states);
            $form->display('created_at','创建时间');
            $form->display('updated_at','更新时间');
        });
    }
    // public function store(){
    //     $data= \Illuminate\Support\Facades\Input::all();
    //     $validator = \Validator::make($data, [
    //         'sku_id' => 'required|unique:sku|max:255',
    //         'name' => 'required|min:2'
    //     ]);
    //     dd($validator->errors());

    //     if ($validator) {
    //         return back()->withInput()->withErrors($validator->errors());
    //     }
    //     // return redirect()->route('skus.index')->with('message', 'Created successfully.');

    // }
    public function update($id)
    {
        return $this->editForm()->update($id);
    }
}
