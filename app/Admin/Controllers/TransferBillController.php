<?php

namespace App\Admin\Controllers;

use App\Models\TransferBill;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;
use Encore\Admin\Widgets\Table;
use Encore\Admin\Widgets\Form as myForm;

class TransferBillController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        // dd(Admin::user()->can('purchase-create'));
        return Admin::content(function (Content $content) {

            $content->header('调拨单');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('调拨单');
            $content->description('');

            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('调拨单');
            $content->description('');

            $content->body($this->form());
        });
    }

    public function destroy($id)
    {
        if ($this->form()->destroy($id)) {
            return response()->json([
                'status'  => true,
                'message' => trans('admin.delete_succeeded'),
            ]);
        } else {
            return response()->json([
                'status'  => false,
                'message' => trans('admin.delete_failed'),
            ]);
        }
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(TransferBill::class, function (Grid $grid) {
            $grid->model()->select('company.name','out_warehouse.name as out_warehouse_name','in_warehouse.name as in_warehouse_name','operation.name as operation_name','transfer_bill.*')
            ->leftjoin('company','company.id','=','transfer_bill.company_id')
            ->leftjoin('warehouse as out_warehouse','out_warehouse.id','=','transfer_bill.out_warehouse_id')
            ->leftjoin('warehouse as in_warehouse','in_warehouse.id','=','transfer_bill.in_warehouse_id')

            ->leftjoin('admin_users as operation','transfer_bill.operater_id','=','operation.username')
            ->where('transfer_bill.company_id',Admin::user()->company_id)
            ->orderBy('transfer_bill.created_at', 'desc');
            $grid->id('调拨单编号')->sortable();
            $grid->name('所属公司');
            $grid->operation_name('调度员');
            $grid->out_warehouse_name('调出仓库编号');
            $grid->in_warehouse_name('调入仓库编号');
            $grid->logistics('物流方式')->display(function($v){
                return HelpersController::getStateValue('Logistics',$v);
            });
            $grid->send_time('发货时间');

            $grid->transfer_bill_out_state('调拨出库状态')->display(function($v){
                return HelpersController::getStateValue('transferBillState',$v);
            });
            $grid->transfer_bill_in_state('调拨入库状态')->display(function($v){
                return HelpersController::getStateValue('transferBillState',$v);
            });
             $grid->review_out_state("出库审核状态")->display(function($v){
                return HelpersController::getStateValue('transferBillReviewState',$v);
            });
            $grid->review_in_state("入库审核状态")->display(function($v){
                return HelpersController::getStateValue('transferBillReviewState',$v);
            });
            $grid->disableExport();//禁用导出按钮

            //$grid->paid_amount('已付金额');
            //$grid->purchase_type('采购单类型');
            //$grid->remark('备注');
            $grid->actions(function ($actions) {
                // append一个操作
                $actions->disableEdit();
                $actions->disableDelete();//禁用删除
                $actions->prepend('<a href="'.\Route('transferBill.show',['id'=>$actions->getKey()]).'"><i class="fa fa-eye"></i></a>');
            });

            $grid->filter(function($filter){
                // 去掉默认的id过滤器
                $filter->disableIdFilter();

                $filter->where(function ($query) {
                    $query->where('transfer_bill.id', 'like', "%{$this->input}%");
                }, '调拨单编号');

                $filter->where(function ($query) {
                    $query->where('company.name', 'like', "%{$this->input}%");
                }, '所属公司');

                $filter->where(function ($query) {
                    $query->where('operation.name','like',"%{$this->input}%");
                }, '调度员');

            });

        });
}

    protected function editForm(){
        return Admin::form(TransferBill::class, function (Form $form) {
            $form->select('in_warehouse_id', '仓库编号')->options(\DB::table('warehouse')->pluck('name','id'));
            $form->select('out_warehouse_id', '仓库编号')->options(\DB::table('warehouse')->pluck('name','id'));

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        Admin::script('
                $(document).off("change",".inventory_id");
                $(document).on("change",".inventory_id", function(e){
                    var bid = $(this).val();
                    var pdom =  $(this).parents(".has-many-fakeModel-form");
                    if(!bid){
                        return;
                    }
                    $.ajax({
                      method: "POST",
                      url: "/admin/outs/batchInfo",
                      data: { bid: bid}
                    }).success(function( msg ) {
                        msg = JSON.parse(msg);
                        if(msg){
                            var html = "";
                            html +="<table class=\'table\'>";
                            html +="<tr><td>入库时间</td><td>"+msg.created_at+"</td><td>可用尺数</td><td>"+msg.over_size+"</td><td>可用张数</td><td>"+msg.over_num+"</td>"
                            html +="<tr><td>厚度</td><td>"+msg.batch_thick+"</td><td>等级</td><td>"+msg.batch_grade+"</td><td>产地</td><td>"+msg.batch_origin+"</td>"
                            html +="<tr><td>颜色</td><td>"+msg.batch_colour+"</td><td>平均尺寸</td><td>"+msg.batch_avg_size+"</td><td>最小尺寸</td><td>"+msg.batch_min_size+"</td>"
                            html +="<tr><td>所在仓库</td><td>"+msg.warehouse_name+"</td><td>仓库库区</td><td>"+msg.warehouse_zone_name+"</td><td>仓库库位</td><td>"+msg.warehouse_location_name+"</td>"

                            html +="</table>";
                            pdom.find(".batch_info").html(html);
                        }else{
                            pdom.find(".batch_info").html("批次拉取出错")
                        }
                    });
                });
        ');
        return Admin::form(TransferBill::class, function (Form $form) {

            $form->datetime('send_time', '出库时间');
            $form->select('out_warehouse_id', '调出仓库编号')->options(\DB::table('warehouse')->pluck('name','id'));
            $form->select('in_warehouse_id', '调入仓库编号')->options(\DB::table('warehouse')->pluck('name','id'));
            $form->select('logistics','物流方式')->options(HelpersController::getStateOptions('Logistics'));
            $form->textarea('remark', '备注');
            $form->hasMany('fakeModel', function (Form\NestedForm $form) {
                $skus= \DB::table('sku')->pluck('name','id');
                $form->select('sku_id','sku')->options($skus)->load('inventory_id', '/admin/helpers/inventory_id');
                $form->select('inventory_id','批次Id')->options(\DB::table('inventory')
                ->where('over_size','>',0)
                ->pluck('inventory_id','inventory.id'));
                $form->currency('size','尺寸')->symbol(' ');
                $form->number('num','张数');
                $form->html("<div class='batch_info'></div>","批次属性");
            });

        });
    }
    public function ajaxSkuOtherInfos(Request $req){
        $arr = $req->all();
        $bid = $arr['bid'];
        return json_encode(\DB::table('inventory')->select(
                'sku.name as sku_name',
                'warehouse.name as warehouse_name',
                'warehouse_zone.name as warehouse_zone_name',
                'warehouse_location.name as warehouse_location_name',
                'inventory.*',
                'batch_grade.name as batch_grade',
                'batch_thick.name as batch_thick',
                'batch_origin.name as batch_origin',
                'batch_colour.name as batch_colour',
                'batch_avg_size.name as batch_avg_size',
                'batch_min_size.name as batch_min_size'
            )

            ->leftjoin('sku','sku.id','=','inventory.sku_id')
            ->leftjoin('warehouse','warehouse.id','=','inventory.warehouse_id')
            ->leftjoin('warehouse_zone','warehouse_zone.id','=','inventory.warehouse_zone_id')
            ->leftjoin('warehouse_location','warehouse_location.id','=','inventory.warehouse_location_id')
            ->leftjoin('batchattribute as batch_grade','inventory.grade','=','batch_grade.id')
            ->leftjoin('batchattribute as batch_thick','inventory.thick','=','batch_thick.id')
            ->leftjoin('batchattribute as batch_origin','inventory.origin','=','batch_origin.id')
            ->leftjoin('batchattribute as  batch_colour','inventory.colour','=','batch_colour.id')
            ->leftjoin('batchattribute as batch_avg_size','inventory.avg_size','=','batch_avg_size.id')
            ->leftjoin('batchattribute as batch_min_size','inventory.min_size','=','batch_min_size.id')
            ->where('inventory.inventory_id',$bid)->first());
    }


    public function store(Request $req){
        $purchase_insert_id = \DB::table('id_model')->where('common_key','tansfer_bill_id')->sharedLock()->value('common_value');
        \DB::table('id_model')->where('common_key','tansfer_bill_id')->increment('common_value',1);
        $id='T'.date('Ymd').str_pad($purchase_insert_id,4,"0",STR_PAD_LEFT);
        if(!$req['fakeModel']){
            admin_toastr('没有产生sku单据','error');
            return back()->withInput();
        }

        if(!$req['out_warehouse_id']){
            admin_toastr('调出仓库未维护','error');
            return back()->withInput();
        }
        if(!$req['in_warehouse_id']){
            admin_toastr('调入仓库未维护','error');
            return back()->withInput();
        }
        if(!isset($req['logistics'])){
            admin_toastr('物流方式未维护','error');
            return back()->withInput();
        }
        if(strtotime($req['send_time']) < strtotime(date("Y-m-d")." 00:00:00")){
                admin_toastr('存在出货时间早于今天的sku详细','error');
            return back()->withInput();
        }
        $purchaseDetails = [];
        // dd($req->all());
        foreach ($req['fakeModel'] as $purchaseDetailArr) {
            if(!$purchaseDetailArr['sku_id']){continue;}
            if($purchaseDetailArr['num']<=0){
                admin_toastr('张数存在空值','error');
                return back()->withInput();
            }
            if($purchaseDetailArr['size']<=0){
                admin_toastr('尺数存在空值','error');
                return back()->withInput();
            }

            $purchaseDetail['transfer_bill_id'] = $id;
            $purchaseDetail['sku_id'] = $purchaseDetailArr['sku_id'];
            $purchaseDetail['num'] = $purchaseDetailArr['num'];
            $purchaseDetail['size'] = $purchaseDetailArr['size'];
            $purchaseDetail['inventory_id'] = $purchaseDetailArr['inventory_id'];
            $purchaseDetail['created_at'] =  date('Y-m-d H:i:s');
            array_push($purchaseDetails, $purchaseDetail);
        }
        if(!$purchaseDetails){
            admin_toastr('没有产生sku单据','error');
            return back()->withInput();
        }
        // $review_state = "CREATED";
        // if($req['type']==0){
        //     if(\DB::table('supplier')->where('id',$req['supplier_id'])->value('type')==1){
        //         $review_state = 'FINANCE_1ST';
        //     }
        // }

        \DB::beginTransaction();

        $purchase = \DB::table('transfer_bill')
            ->insert(
                [
                    'id' => $id,
                    'in_warehouse_id' => $req['in_warehouse_id'],
                    'out_warehouse_id' => $req['out_warehouse_id'],
                    'remark' => $req['remark'],
                    'updated_at' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'company_id' => Admin::user()->company_id,
                    'operater_id' => Admin::user()->username,
                    'send_time' => $req['send_time'],
                    'logistics' => $req['logistics']
                    // 'review_state' => $review_state,
                ]
             );
        // dd($skuPriceInsertArr);
        \DB::table('out_transfer_bill_detail')
            ->insert($purchaseDetails);
        \DB::table('in_transfer_bill_detail')
            ->insert($purchaseDetails);

        \DB::commit();
        admin_toastr(trans('admin.save_succeeded'));

        $url = route('transferBill.index');

        return redirect($url);
    }





    public function show($id)
    {

        $model = new Content();
        $data = \DB::table('transfer_bill')->select('company.name','out_warehouse.name as out_warehouse_name','in_warehouse.name as in_warehouse_name','operation.name as operation_name','reviewer.name as reviewer_name','transfer_bill.*')
            ->leftjoin('company','company.id','=','transfer_bill.company_id')
            ->leftjoin('warehouse as out_warehouse','out_warehouse.id','=','transfer_bill.out_warehouse_id')
            ->leftjoin('warehouse as in_warehouse','in_warehouse.id','=','transfer_bill.in_warehouse_id')
            ->leftjoin('admin_users as reviewer','reviewer.username','=','transfer_bill.reviewer_id')

            ->leftjoin('admin_users as operation','transfer_bill.operater_id','=','operation.username')
            ->where('transfer_bill.id',$id)
            ->first();
        $detail_data = \DB::table('out_transfer_bill_detail')->select('out_transfer_bill_detail.*',
            'sku.name as sku_name',
            'batch_grade.name as batch_grade',
            'batch_thick.name as batch_thick',
            'batch_origin.name as batch_origin',
            'batch_colour.name as batch_colour',
            'batch_avg_size.name as batch_avg_size',
            'batch_min_size.name as batch_min_size')
            ->leftjoin('inventory','inventory.inventory_id','=','out_transfer_bill_detail.inventory_id')
            ->leftjoin('sku','out_transfer_bill_detail.sku_id','=','sku.id')
            ->leftjoin('batchattribute as batch_grade','inventory.grade','=','batch_grade.id')
            ->leftjoin('batchattribute as batch_thick','inventory.thick','=','batch_thick.id')
            ->leftjoin('batchattribute as batch_origin','inventory.origin','=','batch_origin.id')
            ->leftjoin('batchattribute as  batch_colour','inventory.colour','=','batch_colour.id')
            ->leftjoin('batchattribute as batch_avg_size','inventory.avg_size','=','batch_avg_size.id')
            ->leftjoin('batchattribute as batch_min_size','inventory.min_size','=','batch_min_size.id')
            ->where('out_transfer_bill_detail.transfer_bill_id',$id)->get();
        $content= '<div style="width:80%;margin:0 auto;background:#fff;padding:10px;"><div style="text-align:right;font-size:1.5rem;font-weight:700"><a style="" href="/admin/transferBill">返回</a></div><h4 style="text-align:center;">基本信息</h4><hr>';
        $headers = [];
        $rows = [
            ['调拨单编号', $data->id, '物流方式', HelpersController::getStateValue('Logistics',$data->logistics)],
            ['调出仓库',$data->out_warehouse_name,'调入仓库',$data->in_warehouse_name],
            ['所属公司', $data->name, '出库时间',$data->send_time]
        ];
        $table_1 = new Table($headers, $rows);
        $content .= $table_1->render();


        $content .= '<h4 style="text-align:center;">系统信息</h4><hr>';

        // table 2
        $headers = [];
        // foreach ($detail_data as $value) {
        //     $value
        // }
        $rows = [
            ['操作人',$data->operation_name,'订单状态', HelpersController::getStateValue("transferBillState",$data->transfer_bill_out_state)." | ".HelpersController::getStateValue("transferBillState",$data->transfer_bill_in_state) ],
            // ['销售',isset($data->salesman)?$data->salesman:'未维护该客户销售','跟单', isset($data->documentary_name)?$data->documentary_name:'未维护该客户跟单' ],
            ['最后操作人',$data->reviewer_name,'最后操作时间',$data->review_time],
            ['生成时间', $data->created_at,'','']

        ];
        $table2 = new Table($headers, $rows);
        $content .= $table2->render();

        $content .= "<h4 style='text-align:center'>批次信息</h4>";
        $headers = ['批次Id','sku','尺数','张数','等级','厚度','产地','颜色','平均尺寸','最小尺寸'];
        $rows = [];
        foreach($detail_data as $v){
            $row = [$v->inventory_id,$v->sku_name,$v->size, $v->num,$v->batch_grade,$v->batch_thick,$v->batch_origin,$v->batch_colour,$v->batch_avg_size,$v->batch_min_size];
            array_push($rows, $row);
        }
        $table2 = new Table($headers, $rows);
        $content .= $table2->render();

        $content .= "<h4 style='text-align:center'>备注</h4><hr><p style='background:#efefef;padding:8px;'>".(trim($data->remark)?$data->remark:"无")."</p>";


        if(Admin::user()->can('transfer_bill.charge')){
            $content .= '<h4 style="text-align:center;">调拨出库主管审核</h4><hr>';
            switch($data->review_out_state){
                case "CREATED":
                    $form = new myForm();
                    $form->action($data->id.'/auditing');
                    $form->method('POST');
                    $form->disablePjax();
                    // echo view('admin::widgets.form', $form->getVariables())->render();
                    // $form->text('pay_type','支付方式');
                    $form->hidden('review_type')->default('out');
                    $form->plus(["l_button"=>"<input name='reject' class='btn btn-danger' type='submit' value='拒绝' />","r_button"=>"<input name='accept' class='btn btn-success' type='submit' value='通过' />"]);

                    $content .= $form->render();
                break;
                case "FINISHED":
                    $content .="<p style='background:#efefef;padding:8px;text-align:center'>订单已完成</p>";
                break;
                case "INACTIVED":
                    $content .="<p style='background:#efefef;padding:8px;text-align:center'>订单已取消</p>";
                break;
                default:
                    $content .="<p style='background:#efefef;padding:8px;text-align:center'>已审核</p>";
            }
            if($data->transfer_bill_out_state =="PART_OUT" || $data->transfer_bill_out_state =="ALL_OUT") {
                $content .= '<h4 style="text-align:center;">调拨入库主管审核</h4><hr>';
                switch ($data->review_in_state) {
                    case "CREATED":
                        $form = new myForm();
                        $form->action($data->id . '/auditing');
                        $form->method('POST');
                        $form->disablePjax();
                        $form->hidden('review_type')->default('in');
                        // echo view('admin::widgets.form', $form->getVariables())->render();
                        // $form->text('pay_type','支付方式');
                        $form->plus(["l_button" => "<input name='reject' class='btn btn-danger' type='submit' value='拒绝' />", "r_button" => "<input name='accept' class='btn btn-success' type='submit' value='通过' />"]);
                        $content .= $form->render();
                        break;
                    case "FINISHED":
                        $content .= "<p style='background:#efefef;padding:8px;text-align:center'>订单已完成</p>";
                        break;
                    case "INACTIVED":
                        $content .= "<p style='background:#efefef;padding:8px;text-align:center'>订单已取消</p>";
                        break;
                    default:
                        $content .= "<p style='background:#efefef;padding:8px;text-align:center'>已审核</p>";
                }
            }
        }

        $items = [
            'header'      => '调拨单',
            'description' => 'TransferBill',
            'breadcrumb'  => '',
            'content'     => $content."</div>",
        ];

        return view('admin::content', $items)->render();
    }
    public function auditing($id){
        $input = \Illuminate\Support\Facades\Input::all();
        // $validator = \Validator::make($input, [
        //     'pay_type' => 'required',
        //     'payment_state'=>'required',
        //     'paid_amount'=>'required'
        // ]);
        // // dd((boolean)$validator);

        // if ($validator->fails()) {
        //     return back()->withInput()->withErrors($validator->errors());
        // }
        unset($input['_token']);
        $input['reviewer_id'] = Admin::user()->username;
        $input['review_time'] = date('Y-m-d H:i:s');
        $old = \DB::table('transfer_bill')->where('id',$id)->first();
        // if($old->payment_state == 2 && $old->review_state == "FINANCE_LAST"){
        //     admin_toastr("订单已付全款,如要变更请联系系统管理员");
        //     return back()->withInput();
        // }
        if($input['review_type']=="in"){
            if($old->review_in_state == "CHARGE_IN_CHECK"){
                admin_toastr("订单已审核,如要变更请联系系统管理员");
                return back()->withInput();
            }
            if($old->review_in_state == "FINISHED" || $old->review_in_state == "IN_CHECK" ){
                admin_toastr("订单已完成,如要变更请联系系统管理员");
                return back()->withInput();
            }
        }else{
            if($old->review_out_state == "CHARGE_OUT_CHECK"){
                admin_toastr("订单已审核,如要变更请联系系统管理员");
                return back()->withInput();
            }
            if($old->review_out_state == "FINISHED" || $old->review_out_state == "OUT_CHECK"){
                admin_toastr("订单已完成,如要变更请联系系统管理员");
                return back()->withInput();
            }
        }


        if(isset($input['accept'])){
            // 审核通过
            if($input['review_type'] == "out"){
                $input['review_out_state'] = "CHARGE_OUT_CHECK";
            }else{
                $input['review_in_state'] = "CHARGE_IN_CHECK";
            }
            unset($input['review_type']);
        }elseif(isset($input['reject'])){
            // 审核不通过
            if($input['review_type'] == "out"){
                $input['review_out_state'] = "INACTIVED";
            }else{
                $input['review_in_state'] = "INACTIVED";
            }
            unset($input['reject']);
            unset($input['review_type']);
            \DB::beginTransaction();
            try {
                \DB::table('transfer_bill')->where('id',$id)->update($input);
                \DB::table('bills_checking_detail')->insert([
                    'operater_id'=>$input['reviewer_id'],
                    'created_at'=>date('Y-m-d H:i:s'),
                    'checking_ASN'=>$id
                ]);
                \DB::commit();
            }catch (Exception $e) {
                \DB::rollBack();
                admin_toastr("数据库出错，请联系管理员","error");
                return back()->withInput();
            }
            admin_toastr("审核处理成功");
            $url = route('orders.index');
            return redirect($url);

        }else{
            admin_toastr('未知错误，请联系管理员','error');
            return back()->withInput();
        }
    \DB::beginTransaction();
    unset($input['reject']);
    unset($input['accept']);

    try {

        \DB::table('transfer_bill')->where('id',$id)->update($input);
        if($old->review_out_state == "CREATED"){
            $details = \DB::table('out_transfer_bill_detail')->join('inventory','inventory.id','=','out_transfer_bill_detail.inventory_id')->join('transfer_bill','transfer_bill.id','=','out_transfer_bill_detail.transfer_bill_id')->where('transfer_bill_id',$id)->get();
            $arrs = [];

            foreach ($details as  $detail) {
                $merge = ['sku_id' =>$detail->sku_id,
                    'grade' => $detail->grade,
                    'thick' => $detail->thick,
                    'origin' => $detail->origin,
                    'colour' => $detail->colour,
                    'avg_size' => $detail->avg_size,
                    'min_size' => $detail->min_size
                ];
                $arr_out = array_merge(
                [
                    'type'=>5,
                    'num'=>$detail->num,
                    'size'=>$detail->size,
                    'operater_id'=>Admin::user()->username,
                    'checking_ASN'=>$id,
                    'inventory_id'=>$detail->inventory_id,
                    'created_at'=>date('Y-m-d H:i:s')
                ],$merge);
                 $arr_in = array_merge(
                 [
                     'type'=>6,
                     'num'=>$detail->num,
                     'size'=>$detail->size,
                     'operater_id'=>Admin::user()->username,
                     'checking_ASN'=>$id,
                     'inventory_id'=>$detail->inventory_id,
                     'created_at'=>date('Y-m-d H:i:s')
                 ],$merge);
                array_push($arrs,$arr_out);
                array_push($arrs,$arr_in);


                // 调拨出库
                $inventory_checking = array_merge([
                    'type'=>1,
                    'warehouse_id'=>$detail->out_warehouse_id,
                ],$merge);
                if (\DB::table('inventory_checking')->where($inventory_checking)->count()){
                    \DB::table('inventory_checking')->where($inventory_checking)->update(['size'=> \DB::raw('size+'.$detail->size),'num'=> \DB::raw('num+'.$detail->num)]);
                }else{
                    $insert = $inventory_checking;
                    $insert['size'] = $detail->size;
                    $insert['num'] = $detail->num;
                    \DB::table('inventory_checking')->insert($insert);
                }
                // 调拨入库
                $inventory_checking = array_merge([
                    'type'=>2,
                    'warehouse_id'=>$detail->in_warehouse_id,
                ],$merge);
                if (\DB::table('inventory_checking')->where($inventory_checking)->count()){
                    \DB::table('inventory_checking')->where($inventory_checking)->update(['size'=> \DB::raw('size+'.$detail->size),'num'=> \DB::raw('num+'.$detail->num)]);
                }else{
                    $insert = $inventory_checking;
                    $insert['size'] = $detail->size;
                    $insert['num'] = $detail->num;
                    \DB::table('inventory_checking')->insert($insert);
                }
            }
            \DB::table('inventory_checking_detail')->insert($arrs);
        }
        \DB::table('bills_checking_detail')->insert([
                    'operater_id'=>$input['reviewer_id'],
                    'created_at'=>date('Y-m-d H:i:s'),
                    'data'=>json_encode($input),
                    'checking_ASN'=>$id
                ]);
        \DB::commit();


        admin_toastr("审核处理成功");
        $url = route('transferBill.index');
        return redirect($url);
    } catch (Exception $e) {
        \DB::rollBack();
        admin_toastr("数据库出错，请联系管理员","error");
        return back()->withInput();
    }
    }
}