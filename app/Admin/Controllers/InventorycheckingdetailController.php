<?php

namespace App\Admin\Controllers;

use App\Models\Inventorycheckingdetail;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class InventorycheckingdetailController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('操作记录');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('操作记录');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('操作记录');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Inventorycheckingdetail::class, function (Grid $grid) {

            // $grid->model()->select('company.name as company_name','warehouse.*')
            // ->leftjoin('company','company.id','=','warehouse.company_id');

            $grid->model()->select(
                'sku.name as sku_name',
                'operation.name as operation_name',
                'batch_grade_table.name as batch_grade',
                'batch_thick_table.name as batch_thick',
                'batch_origin_table.name as batch_origin',
                'batch_colour_table.name as batch_colour',
                'batch_avg_size_table.name as batch_avg_size',
                'batch_min_size_table.name as batch_min_size',
                'inventory_checking_detail.*')
            ->leftjoin('sku','sku.id','=','inventory_checking_detail.sku_id')
            ->leftjoin('admin_users as operation','inventory_checking_detail.operater_id','=','operation.username')
            ->leftjoin('batchattribute as batch_grade_table','batch_grade_table.id','=','inventory_checking_detail.grade')
            ->leftjoin('batchattribute as batch_thick_table','batch_thick_table.id','=','inventory_checking_detail.thick')
            ->leftjoin('batchattribute as batch_origin_table','batch_origin_table.id','=','inventory_checking_detail.origin')
            ->leftjoin('batchattribute as batch_colour_table','batch_colour_table.id','=','inventory_checking_detail.colour')
            ->leftjoin('batchattribute as batch_avg_size_table','batch_avg_size_table.id','=','inventory_checking_detail.avg_size')
            ->leftjoin('batchattribute as batch_min_size_table','batch_min_size_table.id','=','inventory_checking_detail.min_size')->orderBy('inventory_checking_detail.created_at','desc');



            $grid->sku_name('SKU名称');
            $grid->checking_ASN('涉及单据')->display(function($v){return $v?$v:"-";});
            $grid->type('查询库存状态')->display(function($v){
                //return $v==1?'出库':($v==2?"入库":($v==3?'出库锁定':($v==4?'入库在途':"未知")));
                return HelpersController::getStateValue('inventoryCheckingDetail',$v);
            });
            $grid->num('张数')->display(function($v){return round($v,2);});
            $grid->size('尺寸')->display(function($v){return round($v,2);});

            $grid->operation_name('操作人');
            $grid->inventory_id('批次编号')->display(function($v){return $v?$v:"-";});;
            //$grid->checkingASN('单据');
            //$grid->size('尺寸');
            $grid->batch_grade('等级');
            $grid->batch_origin('产地');
            $grid->batch_thick('厚度');
            $grid->batch_colour('颜色');
            $grid->batch_avg_size('平均尺寸');
            $grid->batch_min_size('最小尺寸');
            //$grid->created_at('创建时间');
            //$grid->updated_at('更新时间');



            $grid->disableCreateButton();//禁用新增按钮
            $grid->disableActions();//禁用删除按钮
            $grid->disableExport();//禁用导出按钮
            $grid->actions(function ($actions) {
            // append prepend
            $actions->disableEdit(); // 禁用编辑
            // $actions->prepend('<a href="'.\Route('ins.show',['id'=>$actions->row['detail_id']]).'"><i class="fa fa-eye"></i></a>');
            });
            $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->where(function ($query) {
                $query->where('sku.name', 'like', "%{$this->input}%");
            }, 'SKU名称');

            $filter->where(function ($query) {
                $query->where('inventory_checking_detail.checking_ASN', 'like', "%{$this->input}%");
            }, '涉及单据');

            $filter->where(function ($query) {
                $query->where('inventory_checking_detail.inventory_id', 'like', "%{$this->input}%");
            }, '批次编号');

            $filter->where(function ($query) {
                $query->where('inventory_checking_detail.type', 'like', "%{$this->input}%");
            }, '查询库存状态');

            $filter->where(function ($query) {
                $query->where('operation.name', 'like', "%{$this->input}%");
            }, '操作人');

            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Inventorycheckingdetail::class, function (Form $form) {

            $form->display('id','编号');
            $form->display('sku_id', 'SKU编号');
            $form->display('type', '查询库存状态');
            $form->display('num', '数量');
            $form->display('operater_id', '操作人');
            $form->display('checking_ASN', '单据');
            $form->display('inventory_id', '库存编号');
            //$form->text('size', '尺寸');
            $form->display('grade', '等级');
            $form->display('origin', '产地');
            $form->display('thick', '厚度');
            $form->display('colour', '颜色');
            $form->display('avg_size', '平均尺寸');
            $form->display('min—_size', '最小尺寸');
            $form->display('created_at', '创建时间');
            $form->display('updated_at', '更新时间');



        });
    }
}
