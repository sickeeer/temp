<?php

namespace App\Admin\Controllers;

use App\Models\Supplier;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SupplierController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('供应商');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('供应商');
            $content->description('');

            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('供应商');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Supplier::class, function (Grid $grid) {
            $grid->model()->select('supplier_s.name as salesman_name','supplier_m.name as merchandiser_name','supplier.*')
            ->leftjoin('admin_users as supplier_m','supplier.merchandiser','supplier_m.username')
            ->leftjoin('admin_users as supplier_s','supplier.salesman','supplier_s.username');
            // $grid->id('编号')->sortable();
            // $grid->disableExport();//禁用导出按钮

            $grid->name('供应商名称');
            $grid->address('地址');
            //  $grid->phone('手机');
            //$grid->payType('付款方式');
            $grid->currency('币种')->display(function ($v) {
                return HelpersController::getStateValue('currency',$v);
            });
            //$grid->isDelete('是否删除');
            $grid->rate('税率')->display(function($v){return $v."%";});
            $grid->salesman_name('采购');
            $grid->merchandiser_name('跟单');
            $grid->status('供应商状态')->display(function ($v) {
                return HelpersController::getStateValue('state',$v);
            });
            //$grid->createTime('创建时间');

             $grid->filter(function($filter){
                // 去掉默认的id过滤器
                $filter->disableIdFilter();

                $filter->where(function ($query) {
                    $query->where('supplier.name', 'like', "%{$this->input}%");
                }, '供应商名称');

                $filter->where(function ($query) {
                    $query->where('supplier_s.name', 'like', "%{$this->input}%");
                }, '采购');

                $filter->where(function ($query) {
                    $query->where('supplier_m.name', 'like', "%{$this->input}%");
                }, '跟单');
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Supplier::class, function (Form $form) {


            $form->tab('基础资料', function ($form) {
                // $form->display('id', '编号');
                $form->text('name', '公司名称 *')->rules('required',['required' => '公司名称必填']);
                $form->text('country','国家');
                $form->text('province','城市');
                $form->text('address','详细地址 *')->rules('required',['required' => '详细地址必填']);

            })->tab('联系方式', function ($form) {
                $form->mobile('phone','手机');
                $form->mobile('telephone','固定电话');
                $form->text('fax','传真');
                $form->text('email','邮箱');
                $form->text('postCode','邮编');

            })->tab('交易信息', function ($form) {
                $form->select('type','类型')->options(HelpersController::getStateOptions('supplierType'));
                //$form->select('supplierStatus','状态')->options(['可用','禁用']);
                $form->select('pay_type','支付方式')->options(HelpersController::getStateOptions('payType'));
                $form->select('currency','币种')->options(HelpersController::getStateOptions('currency'))->rules('required',['required'=>'币种必填']);
                $form->rate('rate','税率')->rules('regex:/^[0-9]\d*(\.\d+)?$/',['regex' => '税率至少大于等于0']);
                //$form->text('isDelete','是否删除');

            })->tab('系统信息', function ($form) {


                $users = \DB::table('admin_users')->pluck('name','username');
                $cg  = \DB::table('admin_users')->leftjoin('admin_role_users','admin_role_users.user_id','admin_users.id')->whereIn('admin_role_users.role_id',[5,8])->pluck('name','username');

                $form->select('salesman','采购')->rules('required',['required' => '采购必填'])->options($cg);
                $form->select('merchandiser','跟单')->rules('required',['required' => '跟单必填'])->options($users);
                //$form->textarea('incoterm2','贸易术语');
                //$form->textarea('alternative1','备选1');
                //$form->textarea('alternative2','备选2');
                //$form->textarea('alternative3','备选3');
            });
        });
    }
    protected function editForm()
    {
        return Admin::form(Supplier::class, function (Form $form) {


            $form->tab('基础资料', function ($form) {
                // $form->display('id', '编号');
                $form->text('name', '公司名称 *')->rules('required',['required' => '公司名称必填']);
                $form->text('country','国家');
                $form->text('province','城市');
                $form->text('address','详细地址 *')->rules('required',['required' => '详细地址必填']);

            })->tab('联系方式', function ($form) {
                $form->mobile('phone','手机');
                $form->mobile('telephone','固定电话');
                $form->text('fax','传真');
                $form->text('email','邮箱');
                $form->text('postCode','邮编');

            })->tab('交易信息', function ($form) {
                $form->select('type','类型')->options(HelpersController::getStateOptions('supplierType'));
                //$form->select('supplierStatus','状态')->options(['可用','禁用']);
                $form->select('pay_type','支付方式')->options(HelpersController::getStateOptions('payType'));
                $form->select('currency','币种')->options(HelpersController::getStateOptions('currency'))->rules('required',['required'=>'币种必填']);
                $form->rate('rate','税率')->rules('regex:/^[0-9]\d*(\.\d+)?$/',['regex' => '税率至少大于等于0']);

                //$form->text('isDelete','是否删除');

            })->tab('系统信息', function ($form) {


                $users = \DB::table('admin_users')->pluck('name','username');
                $cg  = \DB::table('admin_users')->leftjoin('admin_role_users','admin_role_users.user_id','admin_users.id')->whereIn('admin_role_users.role_id',[5,8])->pluck('name','username');

                $form->select('salesman','采购')->rules('required',['required' => '采购必填'])->options($cg);
                $form->select('merchandiser','跟单')->rules('required',['required' => '跟单必填'])->options($users);
                $form->switch('status','供应商状态')->states(json_decode(config('batchAttributeState'),true));
                $form->display('created_at','创建时间');
                $form->display('updated_at','更新时间');
                //$form->textarea('incoterm2','贸易术语');
                //$form->textarea('alternative1','备选1');
                //$form->textarea('alternative2','备选2');
                //$form->textarea('alternative3','备选3');
            });
        });
    }

    public function update($id)
    {
        return $this->editForm()->update($id);
    }
}
