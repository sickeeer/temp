<?php
namespace App\Admin\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Tree;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ModelForm;
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('商品分类');
            $content->body($this->tree());
        });
    }
    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('商品分类');
            $content->body($this->editForm()->edit($id));
        });
    }
    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('商品分类');
            $content->body($this->form());
        });
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function tree()
    {
        return Category::tree(function (Tree $tree) {
            $tree->branch(function ($branch) {
                // $src = config('admin.upload.host') . '/' . $branch['logo'] ;
                // $logo = "<img src='$src' style='max-width:30px;max-height:30px' class='img'/>";
                if($branch['status']){
                    return $branch['title']." <span class='label label-danger'>冻结<span>";
                }else{
                    return $branch['title'];
                }
            });
        });
    }
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        //$attr = json_decode(config("sku_batch_attribute"));
        return Category::form(function (Form $form) {
            $form->select('parent_id','父栏目')->options(Category::selectOptions());
            $form->text('title','商品分类名称 *')->rules('required',['required' => '商品分类名称必填']);
            $form->integer('order','类别排序');
            $form->multipleSelect('batchattribute','绑定属性 *')->options(json_decode(config('batchAttribute'),true))->rules('required',['required' => '绑定属性必填']);

            $form->textarea('remark','栏目说明');
            // $form->image('logo');

        });
    }
    protected function editForm()
    {
        return Category::form(function (Form $form) {
            $form->display('id','栏目编号');
            $form->select('parent_id','父栏目')->options(Category::selectOptions());
            $form->text('title','商品分类名称 *')->rules('required',['required' => '商品分类名称必填']);
            $form->integer('order','类别排序');
            $form->multipleSelect('batchattribute','绑定属性 *')->options(json_decode(config('batchAttribute'),true))->rules('required',['required' => '绑定属性必填']);

            $form->textarea('remark','栏目说明');
            $form->switch('status','商品状态')->states(json_decode(config('batchAttributeState'),true));
            $form->display('created_at','创建时间');
            $form->display('updated_at','更新时间');
        });
    }

    public function update($id)
    {
        return $this->editForm()->update($id);
    }
}