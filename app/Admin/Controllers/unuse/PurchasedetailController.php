<?php

namespace App\Admin\Controllers;

use App\Models\Purchasedetail;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PurchasedetailController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('采购详情');
            $content->description('purchasedetail');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('采购详情');
            $content->description('purchasedetail');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('采购详情');
            $content->description('purchasedetail');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Purchasedetail::class, function (Grid $grid) {

            $grid->purchase_id('采购单编号');
            $grid->purchaseDetail_id('采购详情编号');
            $grid->sku_id('商品sku编号');
            $grid->productPrice('商品单价');
            $grid->productNum('商品数量');
            //$grid->grade('商品等级');
            //$grid->thick('商品厚度');
            //$grid->origin('商品产地');
            //$grid->colour('商品颜色');
            //$grid->avg_size('平均尺寸');
            //$grid->min_size('最小尺寸');
            //$grid->supplierProduct_id('供应商商品编号');
            $grid->arriveNum('到货数量');
            //$grid->unit('单位');
            //$grid->arriveTime('预计到货时间');
            //$grid->created_at('创建时间');
            //$grid->updated_at('更新时间');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Purchasedetail::class, function (Form $form) {

            $form->tab('基础资料', function ($form) {
                $form->text('purchaseDetail_id', '采购详情编号');
                $form->text('supplierProduct_id', '供应商商品编号');
                $form->text('purchase_id', '采购单编号');
                $form->text('sku_id', '商品sku编号');
                $form->text('productPrice', '商品单价');
                $form->text('productNum', '商品数量');
                $form->text('arriveNum', '到货数量');
                $form->text('unit', '单位');
                $form->date('arriveTime', '预计到货时间');
                $form->date('created_at', '创建时间');
                $form->date('updated_at', '更新时间');

            })->tab('商品信息', function ($form) {
                $form->text('grade', '商品等级');
                $form->text('thick', '商品厚度');
                $form->text('origin', '商品产地');
                $form->text('colour', '商品颜色');
                $form->text('avg_size', '平均尺寸');
                $form->text('min_size', '最小尺寸');
            });
        });
    }
}
