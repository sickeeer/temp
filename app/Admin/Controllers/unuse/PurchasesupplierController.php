<?php

namespace App\Admin\Controllers;

use App\Models\Purchasesupplier;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PurchasesupplierController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('供应商采购');
            $content->description('purchasesupplier');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('供应商采购');
            $content->description('purchasesupplier');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('供应商采购');
            $content->description('purchasesupplier');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Purchasesupplier::class, function (Grid $grid) {

            $grid->org_id('采购员顶级组织编号');
            $grid->pusu_id('供应商采购');
            $grid->purchase_id('采购员');
            $grid->supplier_id('供应商');
            //$grid->remark('备注');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Purchasesupplier::class, function (Form $form) {

            $form->text('org_id', '采购员顶级组织编号');
            $form->text('pusu_id', '供应商采购');
            $form->text('purchase_id', '采购员');
            $form->text('supplier_id', '供应商');
            $form->textarea('remark', '备注');
        });
    }
}
