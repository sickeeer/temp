<?php

namespace App\Admin\Controllers;

use App\Models\Inventory_detail;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class InventorydetailController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('库存明细');
            $content->description('inventory_detail');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('库存明细');
            $content->description('inventory_detail');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('库存明细');
            $content->description('inventory_detail');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Inventory_detail::class, function (Grid $grid) {

            $grid->inventoryDetail_id('库存明细编号');
            $grid->sku_id('sku编码');
            $grid->org_id('组织编号');
            $grid->orders_id('单据编号');
            $grid->inventory('在库库存');
            $grid->onOrderInventory('在途库存');
            $grid->remainingInventory('可用在库库存');
            $grid->remainingOnOrderInventory('可用在途库存');
            //$grid->create_time('建立时间');
            //$grid->update_time('修改时间');
            //$grid->estimateDelivery_time('在途预计到货时间');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Inventory_detail::class, function (Form $form) {

            $form->text('inventoryDetail_id', '库存明细编号');
            $form->text('sku_id', 'sku编码');
            $form->text('org_id', '组织编号');
            $form->text('orders_id', '单据编号');
            $form->text('order_type', '单据类型');
            $form->text('inventory', '在库库存');
            $form->text('onOrderInventory', '在途库存');
            $form->text('remainingInventory', '可用在库库存');
            $form->text('remainingOnOrderInventory', '可用在途库存');
            $form->text('create_time', '建立时间');
            $form->text('update_time', '修改时间');
            $form->text('estimateDelivery_time', '在途预计到货时间');
        });
    }
}
