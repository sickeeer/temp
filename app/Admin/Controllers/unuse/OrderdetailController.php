<?php

namespace App\Admin\Controllers;

use App\Models\Orderdetail;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class OrderdetailController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('订单管理');
            $content->description('orderdetail');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('订单管理');
            $content->description('orderdetail');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('订单管理');
            $content->description('orderdetail');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Orderdetail::class, function (Grid $grid) {

            $grid->orderDetail_id('订单详情编号');
            $grid->orders_id('订单编号');
            $grid->productNum('商品数量');
            $grid->sku_id('单品编号');
            //$grid->grade('商品等级');
            //$grid->thick('商品厚度');
            //$grid->origin('商品产地');
            //$grid->colour('商品颜色');
            //$grid->avg_size('平均尺寸');
            //$grid->min_size('最小尺寸');
            $grid->productPrice('商品单价');
            //$grid->created_at('创建时间');
            //$grid->updated_at('更新时间');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Orderdetail::class, function (Form $form) {

            $form->text('orderDetail_id', '订单详情编号');
            $form->text('orders_id', '订单编号');
            $form->text('sku_id', '单品编号');
            $form->text('productNum', '商品数量');
            $form->text('grade', '商品等级');
            $form->text('thick', '商品厚度');
            $form->text('origin', '商品产地');
            $form->text('colour', '商品颜色');
            $form->text('avg_size', '平均尺寸');
            $form->text('min_size', '最小尺寸');
            $form->text('productPrice', '商品单价');
            $form->date('created_at', '创建时间');
            $form->date('updated_at', '更新时间');
        });
    }
}
