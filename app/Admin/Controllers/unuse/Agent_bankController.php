<?php

namespace App\Admin\Controllers;

use App\Models\Agent_bank;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class Agent_bankController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('客户银行');
            $content->description('agent_bank');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('客户银行');
            $content->description('agent_bank');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('客户银行');
            $content->description('agent_bank');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Agent_bank::class, function (Grid $grid) {

            $grid->agentBank_id('客户银行编号');
            $grid->agentBaseInfo_id('客户编号');
            $grid->bankName('银行名称');
            $grid->bankAddress('开户行');
            $grid->accountName('账户名称');
            $grid->account('银行账号');
            //$grid->fpPhone('fpPhone');
            //$grid->fpArea('发票地址');
            $grid->taxpayer_id('纳税人识别号');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Agent_bank::class, function (Form $form) {

            $form->text('agentBank_id', '客户银行编号');
            $form->text('agentBaseInfo_id', '客户编号');
            $form->text('bankAddress', '开户行');
            $form->text('bankName', '银行名称');
            $form->text('accountName', '账户名称');
            $form->text('account', '银行账号');
            $form->text('fpPhone', '发票电话');
            $form->text('fpArea', '发票地址');
            $form->text('taxpayer_id', '纳税人识别号');
        });
    }
}
