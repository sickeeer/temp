<?php

namespace App\Admin\Controllers;

use App\Models\SupplierGoodsPrice;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SupplierGoodsPriceController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('供应商商品价格');
            $content->description('supplierGoodsPrice');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('供应商商品价格');
            $content->description('supplierGoodsPrice');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return redirect(route('supplierGoods.create'));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(SupplierGoods::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->id('商品价格编号');
            $grid->sku_id('商品sku编码');
            $grid->supplier_id('供应商编码');
            $grid->minNum('最小数量');
            $grid->maxNum('最大数量');
            $grid->productPrice('采购价格');
            $grid->timestamps();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Supplier_goodsprice::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->display('supplierGoods_id', '商品价格编号');
            $form->display('sku_id', '商品sku编码');
            $form->display('supplier_id', '供应商编码');
            $form->display('minNum', '最小数量');
            $form->display('maxNum', '最大数量');
            $form->display('productPrice', '采购价格');
            $form->display('createTime', '修改时间');
        });
    }
}
