<?php

namespace App\Admin\Controllers;

use App\Models\Supplierbank;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class supplierbankController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('供应商银行');
            $content->description('supplierbank');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('供应商银行');
            $content->description('supplierbank');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('供应商银行');
            $content->description('supplierbank');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Supplierbank::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->supplierBank_id('供应商银行编号');
            $grid->supplier_id('供应商编号');
            $grid->bankName('银行名称');
            $grid->bankType('开户行');
            $grid->bankAccount('银行账号');
            $grid->accountName('账户名称');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Supplierbank::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('supplierBank_id', '供应商银行编号');
            $form->text('supplier_id', '供应商编号');
            $form->text('bankName', '银行名称');
            $form->text('bankType', '开户行');
            $form->text('bankAccount', '银行账号');
            $form->text('accountName', '账户名称');
        });
    }
}
