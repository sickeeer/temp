<?php

namespace App\Admin\Controllers;

use App\Models\Productattribute;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ProductattributeController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('商品属性');
            $content->description('productattribute');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('商品属性');
            $content->description('productattribute');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('商品属性');
            $content->description('productattribute');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Productattribute::class, function (Grid $grid) {

            $grid->category_id('分类编号');
            $grid->batchattributeCode('批次编号');
            $grid->batchattributeName('批次名称');
            $grid->isDelete('是否删除');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Productattribute::class, function (Form $form) {

            $form->text('category_id', '分类编号');
            $form->text('batchattributeCode', '批次编号');
            $form->text('batchattributeName','批次名称');
            $form->text('isDelete','是否删除');
        });
    }
}
