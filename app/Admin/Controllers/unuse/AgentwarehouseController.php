<?php

namespace App\Admin\Controllers;

use App\Models\Agentwarehouse;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AgentwarehouseController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('客户仓库');
            $content->description('agentwarehouse');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('客户仓库');
            $content->description('agentwarehouse');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('客户仓库');
            $content->description('agentwarehouse');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Agentwarehouse::class, function (Grid $grid) {

            $grid->warehouse_id('客户仓库编号');
            $grid->agentBaseInfo_id('客户编号');
            $grid->country('客户仓库地址(国家)');
            $grid->province('客户仓库地址(城市)');
            $grid->addressInfo('客户仓库的详细地址');
            $grid->consignee('联系人');
            $grid->phone('电话');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Agentwarehouse::class, function (Form $form) {

            $form->text('warehouse_id', '客户仓库编号');
            $form->text('agentBaseInfo_id', '客户编号');
            $form->text('country', '客户仓库地址(国家)');
            $form->text('province', '客户仓库地址(城市)');
            $form->text('addressInfo', '客户仓库的详细地址');
            $form->text('consignee', '联系人');
            $form->text('phone', '电话');
        });
    }
}
