<?php

namespace App\Admin\Controllers;
use App\Models\Company;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CompanyController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('公司管理');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('公司管理');
            $content->description('');

            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('公司管理');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Company::class, function (Grid $grid) {
            $grid->id('公司编号')->sortable();
            $grid->name('公司名称');
            $grid->country('国家');
            $grid->province('城市');
            $grid->address('详细地址');
            $grid->linkman('联系人');
            $grid->tel('联系方式');
            $grid->bankinfo('开户银行');
            $grid->status('公司状态')->display(function ($v) {
                return HelpersController::getStateValue('state',$v);
            });
            //$grid->createTime('创建时间');

             $grid->filter(function($filter){
                // 去掉默认的id过滤器
                $filter->disableIdFilter();

                $filter->where(function ($query) {
                    $query->where('company.name', 'like', "%{$this->input}%");
                }, '公司名称');
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Company::class, function (Form $form) {
            $form->display('id', '编号');
            $form->text('name','公司名称 *')->rules('required',['required' => '公司名称必填']);
            $form->text('country','国家');
            $form->text('province','城市');
            $form->text('address','详细地址 *')->rules('required',['required' => '详细地址必填']);
            $form->text('linkman','联系人');
            $form->mobile('tel','固定电话');
            $form->text('bankinfo','开户银行 *')->rules('required',['required' => '开户银行必填']);
            $form->select('status','商品状态')->options(HelpersController::getStateOptions('state'));
        });
    }

    protected function editForm()
    {
        return Admin::form(Company::class, function (Form $form) {
            $form->display('id', '编号');
            $form->text('name','公司名称 *')->rules('required',['required' => '公司名称必填']);
            $form->text('country','国家');
            $form->text('province','城市');
            $form->text('address','详细地址 *')->rules('required',['required' => '详细地址必填']);
            $form->text('linkman','联系人');
            $form->mobile('tel','固定电话');
            $form->text('bankinfo','开户银行 *')->rules('required',['required' => '开户银行必填']);
            $form->select('status','商品状态')->options(HelpersController::getStateOptions('state'));
            $form->display('created_at','创建时间');
            $form->display('updated_at','更新时间');
        });
    }

    public function update($id)
    {
        return $this->editForm()->update($id);
    }
}
