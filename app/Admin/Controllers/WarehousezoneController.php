<?php

namespace App\Admin\Controllers;

use App\Models\Warehousezone;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class WarehousezoneController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('库区管理');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('库区管理');
            $content->description('');

            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('库区管理');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Warehousezone::class, function (Grid $grid) {
            //$grid->model()->select('operation.name as operation_name','order.*')->leftjoin('admin_users as operation','order.operater_id','=','operation.username');


            $grid->model()->select(
                'warehouse.name as warehouse_name',
                'company.name as company_name',
                'warehouse_zone.*')
            ->leftjoin('warehouse','warehouse.id','=','warehouse_zone.warehouse_id')
            ->leftjoin('company','warehouse.company_id','=','company.id');

            $grid->id('库区编号');
            $grid->company_name('公司名称');
            $grid->warehouse_name('仓库名称');
            $grid->name('库区名称');
            $grid->status('库区状态')->display(function($v){
                return HelpersController::getStateValue('state',$v);
            });
            //$grid->desc('desc');
            //$grid->warehouse_id('仓库编号');

            $grid->disableExport();//禁用导出按钮

            $grid->actions(function ($actions) {
                $actions->disableDelete();//禁用编辑
                //$actions->disableEdit();//禁用删除
            });

            $grid->filter(function($filter){

            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->where(function ($query) {
                $query->where('warehouse_zone.id', 'like', "%{$this->input}%");
            }, '库区编号');

            $filter->where(function ($query) {
                $query->where('warehouse_zone.name', 'like', "%{$this->input}%");
            }, '库区名称');

            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Warehousezone::class, function (Form $form) {

            $form->text('id', '库区编号')->rules('required|unique:warehouse_zone',['required' => '库区编号必填','unique'=>'库区编号已存在']);
            $form->select('warehouse_id', '仓库名称')->options(\DB::table('warehouse')->where(['status'=>0,'company_id'=>Admin::user()->company_id])->pluck('name','id'))->rules('required',['required' => '仓库名称必填']);
            $form->text('name', '库区名称')->rules('required',['required' => '库区名称必填']);
            //$form->switch('status', '库区状态');
            $form->textarea('remark','备注');
            // $states = [
            //             'on'  => ['value' => 0, 'text' => '激活', 'color' => 'success'],
            //             'off' => ['value' => 1, 'text' => '冻结', 'color' => 'danger'],
            //           ];
            $form->switch('status','库区状态')->states(json_decode(config('batchAttributeState'),true));
        });
    }

    protected function editForm()
    {
        return Admin::form(Warehousezone::class, function (Form $form) {

            $form->display('id', '库区编号')->rules('required|unique:warehouse',['required' => '库区编号必填','unique'=>'库区编号已存在']);
            $form->select('warehouse_id', '仓库名称')->options(\DB::table('warehouse')->where(['status'=>0,'company_id'=>Admin::user()->company_id])->pluck('name','id'))->rules('required',['required' => '仓库名称必填']);
            $form->text('name', '库区名称')->rules('required',['required' => '库区名称必填']);
            //$form->switch('status', '库区状态');
            $form->textarea('remark','备注');
            // $states = [
            //             'on'  => ['value' => 0, 'text' => '激活', 'color' => 'success'],
            //             'off' => ['value' => 1, 'text' => '冻结', 'color' => 'danger'],
            //           ];
            $form->switch('status','库区状态')->states(json_decode(config('batchAttributeState'),true));
            $form->display('created_at','创建时间');
            $form->display('updated_at','更新时间');
        });
    }
}
