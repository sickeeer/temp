<?php

namespace App\Admin\Controllers;

use App\Models\SkuPrice;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SkuPriceController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('调价单');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('调价单');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return redirect(route('changePriceBills.create'));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(SkuPrice::class, function (Grid $grid) {
            //$grid->skuPrice_id('编号')->sortable();
            $grid->model()->select('operation.name','sku.name as sku_name','agent_sell_price_bill_detail.*')
            ->leftjoin('agent_sell_price_bill','agent_sell_price_bill.id','=','agent_sell_price_bill_detail.agent_sell_price_bill_id')
            ->leftjoin('admin_users as operation','agent_sell_price_bill.operater_id','=','operation.username')
            ->leftjoin('sku','sku.id','=','agent_sell_price_bill_detail.sku_id')->orderBy('agent_sell_price_bill_detail.created_at', 'desc');
            $grid->agent_sell_price_bill_id('编号');
            $grid->sku_name('商品名称');
            $grid->price('价格')->display(function($v){return round($v,2);});
            $grid->datam('调价对象');
            $grid->datam_type('调价类型')->display(function($v){
                return  HelpersController::getStateValue('datamType',$v);
            });

            $grid->name('操作人');

            $grid->created_at('创建时间');
            // $grid->enable('销售价状态')->display(function($v){
            //     return $v==0?"<span class='label label-success'>生效</span>":"<span class='label label-danger'>取消</span>";
            // });

            $grid->actions(function ($actions) {
                    $actions->disableDelete();//禁用编辑
                    $actions->disableEdit();//禁用删除
            });
            // $grid->disableCreateButton();
            $grid->disableExport();//禁用导出按钮
            $grid->disableActions();//禁用操作列

            $grid->filter(function($filter){
                // 去掉默认的id过滤器
                $filter->disableIdFilter();

                $filter->where(function ($query) {
                    $query->where('agent_sell_price_bill.id', 'like', "%{$this->input}%");
                }, '编号');

                $filter->where(function ($query) {
                    $query->where('sku.name', 'like', "%{$this->input}%");
                }, '商品名称');

                $filter->where(function ($query) {
                    $query->where('operation.name', 'like', "%{$this->input}%");
                }, '操作人');
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(SkuPrice::class, function (Form $form) {

            $form->display('id','编号');
            $form->display('sku_id','单品编号');
            $form->display('datam_id','数据列编号');
            $form->display('datam','数据列冗余列');
            $form->display('datam_type','数据类型');
            $form->display('price','价格');
            $form->display('start_time','有效期开始时间');
            $form->display('end_time','有效期结束时间');
            $form->display('enable','是否有效');
            $form->display('create_time','创建时间');

        });
    }
}
