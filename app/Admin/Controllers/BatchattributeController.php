<?php

namespace App\Admin\Controllers;

use App\Models\Batchattribute;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class BatchattributeController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('批次属性');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('批次属性');
            $content->description('');

            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('批次属性');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Batchattribute::class, function (Grid $grid) {
            $grid->model()->orderBy('batchattribute.created_at','desc');
            $grid->value('批次名称');
            $grid->name('批次值');

            // $grid->batchattribute_id('批次属性编号');
            $grid->parent_id('属性名称')->display(function($v){
                return HelpersController::getStateValue('batchAttribute',$v);
            });


            $grid->status('批次属性状态')->display(function($v){
                return HelpersController::getStateValue('state',$v);
            });

            $grid->disableExport();//禁用导出按钮

            $grid->filter(function($filter){

                // 去掉默认的id过滤器
                $filter->disableIdFilter();

                $filter->where(function ($query) {
                    $query->where('parent_id', $this->input);
                }, '属性名称')->select(
                    [
                        'grade' => '等级',
                        'colour'=>'颜色',
                        'origin'=>'产地',
                        'thick'=>'厚度',
                        'avg_size'=>'平均尺寸',
                        'min_size'=>'最小尺寸',

                    ]);
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Batchattribute::class, function (Form $form) {
            $form->text('value', '批次名称')->rules('required|unique:batchattribute',['required' => '批次名称必填','unique'=>'该批次名称已存在']);
            $form->text('name', '批次值')->rules('required',['required' => '批次名称必填']);
            $form->select('parent_id', '属性名称')->options(HelpersController::getStateOptions('batchAttribute'))->rules('required',['required' => '属性名称必填']);
        });
    }


    protected function editForm()
    {
        return Admin::form(Batchattribute::class, function (Form $form) {
            $form->display('value', '批次名称');
            $form->text('name', '批次值')->rules('required',['required' => '批次值必填']);
            $form->select('parent_id','属性名称')->options(HelpersController::getStateOptions('batchAttribute'))->rules('required',['required' => '属性名称必填']);
            $form->switch('status','批次属性状态')->states(json_decode(config('batchAttributeState'),true));
            $form->display('created_at','创建时间');
            $form->display('updated_at','更新时间');
        });
    }

    public function update($id)
    {
        return $this->editForm()->update($id);
    }

}
