<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class HelpersController extends Controller
{
    /**
     * 级联select 通过仓库库区获取下级仓库库位
     *
     * @param Request $request
     * @return mixed
     */
    public function warehouseLocation(Request $request){
        $q = $request->get('q');

        return \DB::table('warehouse_location')->where(['warehouse_zone_id'=>$q])->get(['id', DB::raw('name as text')]);
    }

    /**
     * 级联select 通过仓库编号获取下级仓库库区
     *
     * @param Request $request
     * @return mixed
     */
    public function warehouseZone(Request $request){
        $q = $request->get('q');
        return \DB::table('warehouse_zone')->where(['warehouse_id'=>$q])->get(['id', DB::raw('name as text')]);
    }

    /**
     * 级联select 通过sku获取存在该sku的批次
     *
     * @param Request $request
     * @return mixed
     */
    public function inventory_id(Request $request){
        $q = $request->get('q');
        return \DB::table('inventory')->where([['over_size','>',0],['inventory.sku_id',$q]])->get([DB::raw('inventory.id as id'), DB::raw('inventory_id as text')]);
    }

    /**
     * 通过配置参数名与配置参数名下的主键 获取配置参数的值
     *
     * @param $name
     * @param $val
     * @param $isNeedHTML
     * @return string
     */
    static public function getStateValue($name,$val,$isNeedHTML=true){
        $arr = json_decode(config($name),true);
        if(!count($arr)){
            return '未知';
        }
        foreach ($arr as $key => $value) {
            if($key == $val){
                if(is_array($value)){
                    if($isNeedHTML){
                        $class = isset($value['class'])?$value['class']:'';
                        return "<span class=\"".$class."\">".$value['value']."</span>";
                    }else{
                        return $value['value'];
                    }
                }else{
                    return $value;
                }
            }
        }
        return "未知";
    }

    /**
     * 通过配置参数名 获取配置参数数组
     *
     * @param $name
     * @return array
     */
    static public function getStateOptions($name){
        $arr = json_decode(config($name),true);
        if(!count($arr)){
            return [];
        }
        $res = [];
        foreach ($arr as $key => $value) {
            if(is_array($value)){
                $res[$key] = $value['value'];
            }else{
                $res[$key] = $value;
            }
        }
        return $res;
    }

    /**
     * 联动id_model表 获取字符串自增序号 组成自增列
     *
     * @param $name
     * @param $prefix
     * @return string
     */
    static public function getId($name,$prefix){
        $insert_id = \DB::table('id_model')->where('common_key',$name)->sharedLock()->value('common_value');
        return $prefix.date('Ymd').str_pad($insert_id,4,"0",STR_PAD_LEFT);
    }

    /**
     * 完成自增调度后 对自增参数进行自增
     * @param $name
     */
    static public function endId($name){
        \DB::table('id_model')->where('common_key',$name)->increment('common_value',1);
    }

//    static public function
}