<?php

namespace App\Admin\Controllers;

use App\Models\Warehouse;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;


use Encore\Admin\Widgets\Table;
use Encore\Admin\Widgets\Form as myForm;


class WarehouseController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('仓库');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('仓库');
            $content->description('');

            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('仓库');
            $content->description('');

            $content->body($this->form());
        });
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Warehouse::class, function (Grid $grid) {

            $grid->model()->select('company.name as company_name','warehouse.*')
            ->leftjoin('company','company.id','=','warehouse.company_id');


            $grid->id('仓库编号')->sortable();
            $grid->company_name('公司名称');
            $grid->name('仓库名称');
            //$grid->province('城市');
            //$grid->country('国家');
            $grid->address('详细地址');
                // $grid->created_at('创建时间');
                // $grid->updated_at('修改时间');
            $grid->status('仓库状态')->display(function($v){
                return HelpersController::getStateValue('state',$v);
            });

            $grid->disableExport();//禁用导出按钮
            // $grid->actions(function ($actions) {
            //     // append prepend
            //     $actions->disableEdit(); // 禁用编辑

            // });

            $grid->actions(function ($actions) {
                    $actions->disableDelete();//禁用编辑
                    //$actions->disableEdit();//禁用删除
                });

            $grid->filter(function($filter){

            // 去掉默认的id过滤器
            $filter->disableIdFilter();


            $filter->where(function ($query) {
                $query->where('warehouse.id', 'like', "%{$this->input}%");
            }, '仓库编号');

            $filter->where(function ($query) {
                $query->where('company.name', 'like', "%{$this->input}%");
            }, '公司名称');

            $filter->where(function ($query) {
                $query->where('warehouse.name', 'like', "%{$this->input}%");
            }, '仓库名称');

            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Warehouse::class, function (Form $form) {


            $form->text('id', '仓库编号')->rules('required|unique:warehouse',['required' => '仓库编号必填','unique'=>'仓库编号已存在']);
            $form->select('company_id', '公司名称')->options(\DB::table('company')->where('status',0)->pluck('name','id'))->rules('required',['required' => '公司名称必填']);
            $form->text('name', '仓库名称')->rules('required',['required' => '仓库编号必填']);
            $form->text('country', '国家');
            $form->text('province', '城市');
            $form->text('address', '详细地址')->rules('required',['required' => '详细地址必填']);
        });
    }

    protected function editForm()
    {
        return Admin::form(Warehouse::class, function (Form $form) {


            $form->display('id', '仓库编号');
            $form->select('company_id', '公司名称')->options(\DB::table('company')->where('status',0)->pluck('name','id'))->rules('required',['required' => '公司编名称必填']);
            $form->text('name', '仓库名称')->rules('required',['required' => '仓库编号必填']);
            $form->text('country', '国家');
            $form->text('province', '城市');
            $form->text('address', '详细地址')->rules('required',['required' => '详细地址必填']);



            $form->textarea('remark', '备注');
            // $states = [
            //             'on'  => ['value' => 0, 'text' => '激活', 'color' => 'success'],
            //             'off' => ['value' => 1, 'text' => '冻结', 'color' => 'danger'],
            //           ];
            $form->switch('status','仓库状态')->states(json_decode(config('batchAttributeState'),true));
            $form->display('created_at','创建时间');
            $form->display('updated_at','更新时间');
        });
    }
    public function update($id)
    {
        return $this->editForm()->update($id);
    }
}
