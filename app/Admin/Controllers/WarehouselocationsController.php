<?php

namespace App\Admin\Controllers;

use App\Models\Warehouselocation;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class WarehouselocationsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('库存位置');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('库存位置');
            $content->description('');

            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('库存位置');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Warehouselocation::class, function (Grid $grid) {

            $grid->model()->select(
                'warehouse_zone.name as warehouse_zone_name',
                'warehouse.name as warehouse_name',
                'company.name as company_name',
                'warehouse_location.*')
            ->leftjoin('warehouse_zone','warehouse_zone.id','=','warehouse_location.warehouse_zone_id')
            ->leftjoin('warehouse','warehouse_zone.warehouse_id','=','warehouse.id')
            ->leftjoin('company','warehouse.company_id','=','company.id');

            $grid->id('库位编号');
            $grid->company_name('公司名称');
            $grid->warehouse_name('仓库名称');
            $grid->warehouse_zone_name('库区名称');
            $grid->name('库位名称');
            $grid->warehouse_location_type('库位类型')->display(function($v){
                return HelpersController::getStateValue('warehouseLocationType',$v);
            });
            //$grid->RFCode('RFCode');
            $grid->status('库位状态')->display(function($v){
                return HelpersController::getStateValue('state',$v);
            });

            $grid->disableExport();//禁用导出按钮

            $grid->actions(function ($actions) {
                $actions->disableDelete();//禁用编辑
                //$actions->disableEdit();//禁用删除
            });

            $grid->filter(function($filter){

            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->where(function ($query) {
                $query->where('warehouse_location.id', 'like', "%{$this->input}%");
            }, '库位编号');

            $filter->where(function ($query) {
                $query->where('warehouse_zone.name', 'like', "%{$this->input}%");
            }, '库位名称');

            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Warehouselocation::class, function (Form $form) {

            $form->text('id', '库位编号')->rules('required|unique:warehouse_location',['required' => '库位编号必填','unique'=>'库位编号已存在']);
            $form->text('name', '库位名称')->rules('required',['required' => '库位名称必填']);
            $form->select('warehouse_zone_id', '库区名称')->options(\DB::table('warehouse_zone')->leftjoin('warehouse','warehouse.id','=','warehouse_zone.warehouse_id')->where(['warehouse_zone.status'=>0,'warehouse.company_id'=>Admin::user()->company_id])->pluck('warehouse_zone.name','warehouse_zone.id'))->rules('required',['required' => '库区名称必填']);
            $form->select('warehouse_location_type', '库位类型')->options(HelpersController::getStateOptions('warehouseLocationType'));
            //$form->text('RFCode', 'RFCode');
            //$form->switch('status', '状态');
            $form->textarea('remark', '备注');
            // $states = [
            //             'on'  => ['value' => 0, 'text' => '激活', 'color' => 'success'],
            //             'off' => ['value' => 1, 'text' => '冻结', 'color' => 'danger'],
            //           ];
            $form->switch('status','库位状态')->states(json_decode(config('batchAttributeState'),true));
            $form->timestamps();
        });
    }

    protected function editForm()
    {
        return Admin::form(Warehouselocation::class, function (Form $form) {

            $form->display('id', '库位编号')->rules('required|unique:warehouse_location',['required' => '库位编号必填','unique'=>'库位编号已存在']);
            $form->text('name', '库位名称')->rules('required',['required' => '库位名称必填']);
            $form->select('warehouse_zone_id', '库区名称')->options(\DB::table('warehouse_zone')->leftjoin('warehouse','warehouse.id','=','warehouse_zone.warehouse_id')->where(['warehouse_zone.status'=>0,'warehouse.company_id'=>Admin::user()->company_id])->pluck('warehouse_zone.name','warehouse_zone.id'))->rules('required',['required' => '库区名称必填']);;
            $form->select('warehouse_location_type', '库位类型')->options(HelpersController::getStateOptions('warehouseLocationType'));
            //$form->text('RFCode', 'RFCode');
            //$form->switch('status', '状态');
            $form->textarea('remark', '备注');
            // $states = [
            //             'on'  => ['value' => 0, 'text' => '激活', 'color' => 'success'],
            //             'off' => ['value' => 1, 'text' => '冻结', 'color' => 'danger'],
            //           ];
            $form->switch('status','状态')->states(json_decode(config('batchAttributeState'),true));
            //$form->timestamps();
            $form->display('created_at','创建时间');
            $form->display('updated_at','更新时间');
        });
    }
}





