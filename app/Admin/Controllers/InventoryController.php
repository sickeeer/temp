<?php

namespace App\Admin\Controllers;

use App\Models\Inventory;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class InventoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('库存');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('库存');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('库存');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Inventory::class, function (Grid $grid) {
             $grid->model()->select(
                'sku.name as sku_name',
                'warehouse.name as warehouse_name',
                'warehouse_zone.name as warehouse_zone_name',
                'warehouse_location.name as warehouse_location_name',
                'batch_grade_table.name as batch_grade',
                'batch_thick_table.name as batch_thick',
                'batch_origin_table.name as batch_origin',
                'batch_colour_table.name as batch_colour',
                'batch_avg_size_table.name as batch_avg_size',
                'batch_min_size_table.name as batch_min_size',
                'inventory.*')
            ->leftjoin('batchattribute as batch_grade_table','batch_grade_table.id','=','inventory.grade')
            ->leftjoin('batchattribute as batch_thick_table','batch_thick_table.id','=','inventory.thick')
            ->leftjoin('batchattribute as batch_origin_table','batch_origin_table.id','=','inventory.origin')
            ->leftjoin('batchattribute as batch_colour_table','batch_colour_table.id','=','inventory.colour')
            ->leftjoin('batchattribute as batch_avg_size_table','batch_avg_size_table.id','=','inventory.avg_size')
            ->leftjoin('batchattribute as batch_min_size_table','batch_min_size_table.id','=','inventory.min_size')
            ->leftjoin('sku','sku.id','=','inventory.sku_id')
            ->leftjoin('warehouse','warehouse.id','=','inventory.warehouse_id')
            ->leftjoin('warehouse_zone','warehouse_zone.id','=','inventory.warehouse_zone_id')
            ->leftjoin('warehouse_location','warehouse_location.id','=','inventory.warehouse_location_id')->orderBy('inventory.created_at', 'desc');



            $grid->inventory_id('批次编号');
            // $grid->contract_id('合同订单编号');
            // $grid->original_id('原始单号');
            // $grid->cowhide_id('皮源编号');

            // $grid->lockedCnt('锁定库存');
            $grid->sku_name('SKU名称');
            $grid->over_size('可用库存(尺数)')->display(function($v){return round($v,2);});
            $grid->over_num('可用库存(张数)')->display(function($v){return round($v,2);});
            if(Admin::user()->isRole('Finance Management')){
                $grid->price('单价')->display(function($v){return round($v,2);});
            }

            $grid->batch_grade('等级');
            $grid->batch_origin('产地');
            $grid->batch_thick('厚度');
            $grid->batch_colour('颜色');
            $grid->batch_avg_size('平均尺寸');
            $grid->batch_min_size('最小尺寸');
            $grid->warehouse_name('仓库名称');
            $grid->warehouse_zone_name('库区名称');
            $grid->warehouse_location_name('库位名称');


            $grid->disableCreateButton();//禁用新增按钮
            $grid->disableExport();//禁用导出按钮
            $grid->disableActions();//禁用删除按钮

            $grid->actions(function ($actions) {
                // append prepend
                $actions->disableEdit(); // 禁用编辑
                // $actions->prepend('<a href="'.\Route('ins.show',['id'=>$actions->row['detail_id']]).'"><i class="fa fa-eye"></i></a>');
            });

            $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->where(function ($query) {
                $query->where('inventory.inventory_id', 'like', "%{$this->input}%");
            }, '批次编号');

            $filter->where(function ($query) {
                $query->where('sku.name', 'like', "%{$this->input}%");
            }, 'SKU名称');

            $filter->where(function ($query) {
                $query->where('warehouse.name', 'like', "%{$this->input}%");
            }, '仓库名称');

            $filter->where(function ($query) {
                $query->where('warehouse_zone.name', 'like', "%{$this->input}%");
            }, '库区名称');

            $filter->where(function ($query) {
                $query->where('warehouse_location.name', 'like', "%{$this->input}%");
            }, '库位名称');

            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Inventory::class, function (Form $form) {
            $form->tab('基础资料', function ($form) {
            $form->text('id', '批次编号')->rules('required',['required' => '批次编号必填'])->attribute('required','required');
            // $form->text('contract_id', '合同订单编号');
            // $form->text('original_id', '原始单号');
            // $form->text('cowhide_id', '皮源编号');

            $form->text('lockedCnt', '锁定库存');
            $form->text('overCnt', '可用库存');
            $form->text('sku_id', 'SKU编号')->rules('required',['required' => 'SKU编号必填'])->attribute('required','required');
            $form->text('warehouse_zone_id', '库位编号');
            $form->text('warehouse_location_id', '位置编号');
            $form->text('warehouse_id', '仓库编号');

            })->tab('规格信息', function ($form) {
                $form->text('grade','等级');
                $form->text('thick','厚度');
                $form->text('origin','产地');
                $form->text('colour','颜色');
                $form->text('min_size','最小尺寸');
                $form->text('avg_size','平均尺寸');
                $form->datetime('created_at', '创建时间');
                $form->datetime('updated_at', '更新时间');
            });
        });
    }
}
