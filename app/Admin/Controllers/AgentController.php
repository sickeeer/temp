<?php

namespace App\Admin\Controllers;

use App\Models\Agent;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AgentController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('客户');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('客户');
            $content->description('');

            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('客户');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Agent::class, function (Grid $grid) {

            $grid->model()->select(
                'agent_s.name as salesman_name',
                'agent_m.name as merchandiser_name',
                'agent.*')
            ->leftjoin('admin_users as agent_m','agent.merchandiser','agent_m.username')
            ->leftjoin('admin_users as agent_s','agent.salesman','agent_s.username')->orderBy('agent.created_at','desc');

            // $grid->id('编号')->sortable();
            $grid->name('客户名称');
            $grid->country('国家');
            $grid->province('城市');
            $grid->address('详细地址');
            $grid->type('客户类型')->display(function ($v) {
                return HelpersController::getStateValue('agentType',$v);
            });
            $grid->customer_source('客户来源')->display(function ($v) {
                return HelpersController::getStateValue('customerSource',$v);
            });
            $grid->salesman_name('销售');
            $grid->merchandiser_name('跟单员');
            $grid->status('客户状态')->display(function($v){
                return HelpersController::getStateValue('state',$v);
            });
            $grid->html('<div class="btn-group pull-right" style="margin-right: 10px">
    <a href="/admin/agents/create2" class="btn btn-sm btn-success">
        <i class="fa fa-save"></i>&nbsp;&nbsp;新增外协客户
    </a>
</div>');

            $grid->disableExport();//禁用导出按钮

            $grid->filter(function($filter){

                // 去掉默认的id过滤器
                $filter->disableIdFilter();

                $filter->where(function ($query) {
                    $query->where('agent.name', 'like', "%{$this->input}%");
                }, '客户名称');

                $filter->where(function ($query) {
                    $query->where('agent_s.name', 'like', "%{$this->input}%");
                }, '采购');

                $filter->where(function ($query) {
                    $query->where('agent_m.name', 'like', "%{$this->input}%");
                }, '跟单');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Agent::class, function (Form $form) {
            $form->tab('基础资料', function ($form) {

                $form->text('name', '客户名称 *')->rules('required',['required' => '客户名称必填']);
                $form->select('type','客户类型')->options(HelpersController::getStateOptions('agentType'))->rules('required',['required' => '客户类型必填']);
                $form->text('country','国家');
                $form->text('province','城市');
                $form->text('address','详细地址 *')->rules('required',['required' => '详细地址必填']);

            })->tab('联系方式', function ($form) {
                $form->text('contacts_person','联系人');
                $form->mobile('phone','手机号码');
                $form->text('email','邮箱');
                $form->text('faxes','传真');
                //$form->text('logistics_type','物流方式');

            })->tab('开票信息', function ($form) {
                $form->rate('out_tax','销项税率')->rules('regex:/^[0-9]\d*(\.\d+)?$/',['regex' => '销项税率大于等于0']);
                $form->text('taxpayer_id','纳税人识别号');
                $form->text('fp_area','发票地址');
                $form->mobile('fp_phone','发票电话');

            })->tab('系统信息', function ($form) {

                $form->select('customer_source','客户来源')->options(HelpersController::getStateOptions('customerSource'));
                $users = \DB::table('admin_users')->where('status',0)->pluck('name','username');
                $xs  = \DB::table('admin_users')->leftjoin('admin_role_users','admin_role_users.user_id','admin_users.id')->whereIn('admin_role_users.role_id',[4,7])->pluck('name','username');

                $form->select('salesman','销售')->rules('required',['required' => '销售必填'])->options($xs);
                $form->select('merchandiser','跟单')->rules('required',['required' => '跟单必填'])->options($users);
                $form->textarea('remark','备注');
            });
        });
    }


    protected function editForm()
    {
        return Admin::form(Agent::class, function (Form $form) {
            $form->tab('基础资料', function ($form) {
                $form->text('name', '客户名称 *')->rules('required',['required' => '客户名称必填']);
                $form->select('type','客户类型')->options(HelpersController::getStateOptions('agentType'))->rules('required',['required' => '客户类型必填']);
                $form->text('country','国家');
                $form->text('province','城市');
                $form->text('address','详细地址 *')->rules('required',['required' => '详细地址必填']);

            })->tab('联系方式', function ($form) {
                $form->text('contacts_person','联系人');
                $form->mobile('phone','手机号码');
                $form->text('email','邮箱');
                $form->text('faxes','传真');
                //$form->text('logistics_type','物流方式');

            })->tab('开票信息', function ($form) {
                $form->rate('out_tax','销项税率')->rules('regex:/^[0-9]\d*(\.\d+)?$/',['regex' => '销项税率大于等于0']);
                $form->text('taxpayer_id','纳税人识别号');
                $form->text('fp_area','发票地址');
                $form->mobile('fp_phone','发票电话');

            })->tab('系统信息', function ($form) {
                $form->select('customer_source','客户来源')->options(HelpersController::getStateOptions('customerSource'));

                $users = \DB::table('admin_users')->pluck('name','username');
                $cg  = \DB::table('admin_users')->leftjoin('admin_role_users','admin_role_users.user_id','admin_users.id')->whereIn('admin_role_users.role_id',[4,7])->pluck('name','username');

                $form->select('salesman','销售')->rules('required',['required' => '销售必填'])->options($cg);
                $form->select('merchandiser','跟单')->rules('required',['required' => '跟单必填'])->options($users);
                $form->textarea('remark','备注');
                $form->switch('status','客户状态')->states(json_decode(config('batchAttributeState'),true));
                $form->display('created_at','创建时间');
                $form->display('updated_at','更新时间');
            });
        });
    }
    protected function edit2Form()
    {
        return Admin::form(Agent::class, function (Form $form) {
            $form->tab('基础资料', function ($form) {
                $form->text('supplier_name', '名称 *')->rules('required',['required' => '名称必填']);
                $form->text('agent_name', '名称 *')->rules('required',['required' => '名称必填']);

                $form->select('type','客户类型')->options(HelpersController::getStateOptions('agentType'))->rules('required',['required' => '客户类型必填']);
                $form->text('country','国家');
                $form->text('province','城市');
                $form->text('address','详细地址 *')->rules('required',['required' => '详细地址必填']);

            })->tab('联系方式', function ($form) {
                $form->text('contacts_person','联系人');
                $form->mobile('phone','手机号码');
                $form->text('email','邮箱');
                $form->text('faxes','传真');
                //$form->text('logistics_type','物流方式');

            })->tab('开票信息', function ($form) {
                $form->rate('out_tax','销项税率')->rules('regex:/^[0-9]\d*(\.\d+)?$/',['regex' => '销项税率大于等于0']);
                $form->rate('in_tax','进项税率')->rules('regex:/^[0-9]\d*(\.\d+)?$/',['regex' => '进项税率大于等于0']);

                $form->text('taxpayer_id','纳税人识别号');
                $form->text('fp_area','发票地址');
                $form->mobile('fp_phone','发票电话');

            })->tab('系统信息', function ($form) {
                $form->select('customer_source','客户来源')->options(HelpersController::getStateOptions('customerSource'));

                $users = \DB::table('admin_users')->pluck('name','username');
                $cg  = \DB::table('admin_users')->whereIn('admin_role_users.role_id',[4,7])->pluck('name','username');

                $form->select('salesman','销售')->rules('required',['required' => '销售必填'])->options($cg);
                $form->select('merchandiser','跟单')->rules('required',['required' => '跟单必填'])->options($users);
                $form->textarea('remark','备注');
                $form->switch('status','客户状态')->states(json_decode(config('batchAttributeState'),true));
                $form->display('created_at','创建时间');
                $form->display('updated_at','更新时间');
            });
        });
    }
    public function update($id)
    {
        return $this->editForm()->update($id);
    }
}
