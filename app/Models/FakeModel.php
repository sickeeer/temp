<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/*
    for hasMany
*/
class FakeModel extends Model
{
    protected $table = 'migrations';
}
