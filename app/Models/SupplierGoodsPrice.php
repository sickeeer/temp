<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierGoodsPrice extends Model
{
    protected $table = 'supplier_goods_price';
    public function supplierGoods()
    {
        return $this->belongsTo(SupplierGoods::class);
    }
}
