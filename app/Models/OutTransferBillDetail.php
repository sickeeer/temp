<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OutTransferBillDetail extends Model
{
     protected $table = 'out_transfer_bill_detail';
     protected $primaryKey = 'id';
}
