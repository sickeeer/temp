<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table = 'purchase';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    public function fakeModel()
    {
        return $this->hasMany(FakeModel::class);
    }
}
