<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InTransferBillDetail extends Model
{
    protected $table = 'in_transfer_bill_detail';
    protected $primaryKey = 'id';
}
