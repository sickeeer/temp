<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Request;

class Order extends Model
{
    protected $table = 'order';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    public function fakeModel()
    {
        return $this->hasMany(FakeModel::class);
    }
}
