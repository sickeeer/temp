<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchasedetail extends Model
{
    protected $table = 'purchase_detail';
    protected $primaryKey = 'id';
}
