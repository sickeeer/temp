<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierGoods extends Model
{
    protected $table = 'supplier_goods_price_bill';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function fakeModel()
    {
        return $this->hasMany(FakeModel::class);
    }
}
