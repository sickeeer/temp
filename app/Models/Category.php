<?php
namespace App\Models;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
class Category extends Model
{
    use ModelTree, AdminBuilder;
    protected $table = 'category';
    public function setBatchattributeAttribute($attrs)
    {
        if (is_array($attrs)) {
            $this->attributes['batchattribute'] = json_encode($attrs);
        }
    }
    public function getBatchattributeAttribute($attrs)
    {
        return json_decode($attrs);
    }
}