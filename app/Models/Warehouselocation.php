<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouselocation extends Model
{
    protected $table = 'warehouse_location';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
}
