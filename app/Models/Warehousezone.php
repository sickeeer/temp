<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehousezone extends Model
{
    protected $table = 'warehouse_zone';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
}
