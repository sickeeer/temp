<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkuPrice extends Model
{
    protected $table = 'agent_sell_price_bill_detail';
    protected $primaryKey = 'agent_sell_price_bill_detail_id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function changePriceBill()
    {
        return $this->belongsTo(ChangePriceBill::class);
    }
}
