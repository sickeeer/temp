<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sku extends Model
{
    protected $table = 'sku';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    public function getBatchattributeAttribute($attrs)
    {
        return json_decode($attrs);
    }
    public function setBatchattributeAttribute($attrs)
    {
        if (is_array($attrs)) {
            $this->attributes['batchattribute'] = json_encode($attrs);
        }
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
