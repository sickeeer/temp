<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangePriceBill extends Model
{
    protected $table = 'agent_sell_price_bill';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function skuPrice()
    {
        return $this->hasMany(SkuPrice::class);
    }
}
