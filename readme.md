## TODO list ##

- 单据 PDF导出以及打印 * purchase(ok) order() in () out ()
- 外协出库后自动生成外协入库单  * ()
- 不允许一张单据中出现批次完全一致的sku * (? 在checking_detail表中多维护一个detail_id 用以找到唯一的detail行，新增时无法找到detail_id 在出入库时候增加)
- 定时任务 每月每日？ * ()
- 采购的公司要分为(内部&外部),内部采购可以不通过财务审核 * 财务初审 (ok) 财务终审 （？） 
- 调拨入库数量不能大于调拨出库的数量 * ()
- 新增一个 只维护采购价的 角色 维护单据采购价 * ()
- 建立数据权限配置 让用户可以跨company查看数据 操作数据 * () 
- 财务可以在一审 维护 汇率 新增单据时 可以实时计算汇率单价 * () 1
- 新增合同编号 原始单号 皮源编号 * ()  优化成独立附表 *** () 2

- 正式库重新配置迁移回去 ** ()


- sku 库存明细一览 *** ()
- 制作手机版看板 *** ()
- 安全库存 *** ()
- 报表以及导出操作 *** ()
- show 操作下 返回上一页 *** purchase (ok) order() in () out ()
- 筛选功能 **** (ok) 细节优化 ()
- 登录禁止被冻结用户 **** ()
- 获取绑定批次属性规则 添加对父路径的拉取 **** ()
- 财务 付款 收款明细 **** 逻辑 (ok) 展现 ()
- 出入库批量操作 **** ()
- 批次属性详细划分 **** ()
- controller、model、表字段 命名规范 路由规范 **** ()
- 国家、城市 维护 **** ()
- 实际出入库的数量是否允许修改 **** ()
- 售后 **** ()
- 盘点 **** ()
- 删除处理 **** ()
- 采购价销售价 无效化处理 **** ()
- 单据报错返回优化。js未执行等 **** ()
- 所有订单号给予正则编辑 **** ()
- inventory_checking_detail表 在入库时候无法跟踪批次sn inventory_sn **** ()


## DONE ##
- 批次属性生成规则
- 入库后添加财务审核
- 张数处理
- 入库 重新结算订单金额
- 全部出/入库 导致 整个订单标识为全部出/入库
- 主管审核
- 全局配置
- 出库不得超过库存处理
- 出库 批次激活状态时，对批次属性的展示
- 因pjax导致 ajax事件注册多次 被执行多次
- 编辑权限PUT  有误  应修正为 /xxx/*
- purchase order 重构 新增 checking表 计算库存锁占
- 公司维护
- 调拨单 重构

## restful
<table>
    <tr>
        <td>method</td><td>remark</td><td>view</td><td>action</td><td>show.route</td>
    </tr>
    <tr>
        <td>Patch</td><td>更新</td><td>edit</td><td>update</td><td>{id}/edit</td>
    </tr>
    <tr>
        <td>Post</td><td>新增</td><td>create</td><td>store</td><td>/create</td>
    </tr>
    <tr>
        <td>POST</td><td>财务审核</td><td>show</td><td>finance</td><td>/{id}</td>
    </tr>
    <tr>
        <td>POST</td><td>主管审核</td><td>show</td><td>auditing</td><td>/{id}</td>
    </tr>
    <tr>
        <td>GET</td><td>pdf导出</td><td>pdf</td><td>pdf</td><td>/pdf/{id}</td>
    </tr>
    <tr>
        <td>Put</td><td>覆盖更新</td><td> - </td><td> - </td><td> - </td>
    </tr>
</table>

## note

```
当前用户权限（不建议使用） Admin::user()->allPermissions()


Admin::user()->isRole('developer');
Admin::user()->can('create-post');
Admin::user()->inRoles(['editor', 'developer']);



// 禁用批量删除操作
$grid->tools(function ($tools) {
    $tools->batch(function ($batch) {
        $batch->disableDelete();
    });
});
$grid->disableCreateButton();//禁用创建按钮
$grid->disableExport();//禁用导出按钮
$grid->disableDelete();//禁用删除
$grid->disableActions();//禁用整个操作td
$grid->actions(function ($actions) {
    $actions->disableDelete();//禁用删除

    // append prepend
    $actions->disableEdit(); // 禁用编辑
    $actions->prepend('<a href="'.\Route('ins.show',['id'=>$actions->row['detail_id']]).'"><i class="fa fa-eye"></i></a>');
});


$states = [
    'on'  => ['value' => 0, 'text' => '激活', 'color' => 'success'],
    'off' => ['value' => 1, 'text' => '冻结', 'color' => 'danger'],
];
$form->switch('status','状态')->states($states);
$form->display('created_at','创建时间');
$form->display('updated_at','更新时间');


$grid->status('状态')->display(function($v){
    switch ($v) {
        case 0:
            $res ='<span class="label label-success">激活</span>';
            break;
        default:
            $res ='<span class="label label-danger">冻结</span>';
        break;
    }
    return $res;
});


$filter->where(function ($query) {

    $query->where('order.id', 'like', "%{$this->input}%");


}, '订单编号');


$filter->where(function ($query) {

    $query->where('company.name', 'like', "%{$this->input}%");


}, '公司名')->select(['商丘' => '商丘']);



composer dump-autoload

php artisan admin:import config
php artisan admin:import helpers
php artisan admin:import scheduling
php artisan vendor:publish 提取配置文件 


pdf

Move the binaries to a path that is not in a synced folder, for example:

cp vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64 /usr/local/bin/
cp vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64 /usr/local/bin/


new  

cp vendor/h4cc/wkhtmltoimage-amd64 /usr/local/bin/
cp vendor/h4cc/wkhtmltopdf-amd64 /usr/local/bin/



and make it executable:

chmod +x /usr/local/bin/wkhtmltoimage-amd64 
chmod +x /usr/local/bin/wkhtmltopdf-amd64

For example, when loaded with composer, the line should look like:

'binary' => base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'),
If you followed the vagrant steps, the line should look like:

'binary'  => '/usr/local/bin/wkhtmltopdf-amd64',
```