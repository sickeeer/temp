<?php

namespace Encore\Admin\Controllers;

use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Permission;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Routing\Controller;
use DB;

class UserController extends Controller
{
    use ModelForm;
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header(trans('admin.administrator'));
            $content->description(trans('admin.list'));
            $content->body($this->grid()->render());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header(trans('admin.administrator'));
            $content->description(trans('admin.edit'));
            $content->body($this->editForm()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header(trans('admin.administrator'));
            $content->description(trans('admin.create'));
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Administrator::grid(function (Grid $grid) {
            $grid->id('ID')->sortable();
            $grid->username(trans('admin.username'));
            $grid->name(trans('admin.name'));
            $grid->roles(trans('admin.roles'))->pluck('name')->label();
            $grid->created_at(trans('admin.created_at'));
            $grid->updated_at(trans('admin.updated_at'));
            $grid->status('状态')->display(function($v){
                switch ($v) {
                    case 0:
                        $res ='<span class="label label-success">激活</span>';
                        break;
                    default:
                        $res ='<span class="label label-danger">冻结</span>';
                        break;
                }
                return $res;
            });
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                if ($actions->getKey() == 1) {
                    $actions->disableDelete();
                }
            });

            $grid->tools(function (Grid\Tools $tools) {
                $tools->batch(function (Grid\Tools\BatchActions $actions) {
                    $actions->disableDelete();
                });
            });
        });
    }



    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Administrator::form(function (Form $form) {

            $form->text('username', trans('admin.username'))->rules('required|unique:admin_users',['required' => '用户名必填','unique'=>'用户名已存在']);
            $form->text('name', trans('admin.name'))->rules('required');
            $form->text('tel', '联系方式')->rules('required',['required' => '联系方式必填'])->help('输入手机号码');
            $form->text('email', '电子邮件');
            $companies = DB::table('company')->get();
            foreach ($companies as $company) {
                $companyOption[$company->id]=$company->name.'('.$company->address.')';
            }

            $form->select('company_id',"所属公司")->options($companyOption)->rules('required',['required' => '公司必填']);

            $form->image('avatar', trans('admin.avatar'));
            $form->password('password', trans('admin.password'))->rules('required|confirmed');
            $form->password('password_confirmation', trans('admin.password_confirmation'))->rules('required')
                ->default(function ($form) {
                    return $form->model()->password;
                });

            $form->ignore(['password_confirmation']);

            $form->multipleSelect('roles', trans('admin.roles'))->options(Role::all()->pluck('name', 'id'));
            $form->multipleSelect('permissions', trans('admin.permissions'))->options(Permission::all()->pluck('name', 'id'));

            $form->saving(function (Form $form) {
                if ($form->password && $form->model()->password != $form->password) {
                    $form->password = bcrypt($form->password);
                }
            });
        });
    }

    public function editForm()
    {
        return Administrator::form(function (Form $form) {

            $form->display('id', 'ID');

            $form->dispaly('username', trans('admin.username'));
            $form->text('name', trans('admin.name'))->rules('required');
            $form->text('tel', '联系方式')->rules('required')->help('输入手机号码');
            $form->text('email', '电子邮件');
            $companies = DB::table('company')->get();
            foreach ($companies as $company) {
                $companyOption[$company->id]=$company->name.'('.$company->address.')';
            }

            $form->select('company_id',"所属公司")->options($companyOption)->rules('required');

            $form->image('avatar', trans('admin.avatar'));
            $form->password('password', trans('admin.password'))->rules('required|confirmed');
            $form->password('password_confirmation', trans('admin.password_confirmation'))->rules('required')
                ->default(function ($form) {
                    return $form->model()->password;
                });

            $form->ignore(['password_confirmation']);

            $form->multipleSelect('roles', trans('admin.roles'))->options(Role::all()->pluck('name', 'id'));
            $form->multipleSelect('permissions', trans('admin.permissions'))->options(Permission::all()->pluck('name', 'id'));
            $states = [
                'on'  => ['value' => '0', 'text' => '激活', 'color' => 'success'],
                'off' => ['value' => '1', 'text' => '冻结', 'color' => 'danger'],
            ];
            $form->switch('status','状态')->states($states);
            $form->display('created_at', trans('admin.created_at'));
            $form->display('updated_at', trans('admin.updated_at'));

            $form->saving(function (Form $form) {
                if ($form->password && $form->model()->password != $form->password) {
                    $form->password = bcrypt($form->password);
                }
            });
        });
    }

    public function update($id)
    {
        return $this->editForm()->update($id);
    }
}
